import { PersonFormComponent } from './person-form/person-form.component'
import { PersonSelectorComponent } from './person-selector/person-selector.component'
import { topnavComponents } from './person-topnav'

export const components: any[] = [
  topnavComponents,
  PersonFormComponent,
  PersonSelectorComponent
]

export * from './person-topnav'
export * from './person-form/person-form.component'
export * from './person-selector/person-selector.component'
