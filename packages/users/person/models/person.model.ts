/**
 *
 */
import { Entity } from '@inzicht/core'

export class Person extends Entity {
  // Afkorting van de academie
  academy: string

  // De naam van de opleiding
  education: string

  // De afkorting van de opleiding, max 3 letters (E, M, TI, BIM, I, ...)
  educationAbbrev: string

  firstName: string

  lastName: string

  emailAddress: string

  constructor(values: any) {
    super(values)
    try {
      this.academy = values['academy'] || 'AE&I'
      this.education = values['education'] || 'Informatica'
      this.educationAbbrev = values['educationAbbrev'] || 'I'
      this.firstName = values['firstName']
      this.lastName = values['lastName']
      this.emailAddress = values['emailAddress']
    } catch (e) {
      console.error('Error in Person constructor: ' + e.toString())
    }
  }

  public get fullName() {
    console.log('fullName', this.firstName)
    return this.firstName + ' ' + this.lastName
  }
}
