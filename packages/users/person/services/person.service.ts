import { Injectable } from '@angular/core'
import { Person } from '../models/person.model'
import { EntityService } from '@inzicht/core'
import { environment } from '@inzicht/inzicht'
import { HttpClient } from '@angular/common/http'

@Injectable()
export class PersonService extends EntityService<Person> {
  constructor(httpClient: HttpClient) {
    super(httpClient, environment.API_BASE_URL, 'persons')
  }
}
