import { PersonEffects } from './person.effect'

export const effects: any[] = [PersonEffects]

export * from './person.effect'
