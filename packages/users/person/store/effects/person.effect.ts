import { Injectable } from '@angular/core'
import { Actions, ofType, Effect } from '@ngrx/effects'
import { switchMap, map, catchError } from 'rxjs/operators'
import { of } from 'rxjs'

import * as fromRouter from '@inzicht/core/router'
import * as personActions from '../actions'
import * as fromServices from '../../services'
import * as fromConstants from '../../person.constants'

@Injectable()
export class PersonEffects {
  constructor(private actions$: Actions, private personService: fromServices.PersonService) {}

  @Effect()
  loadPersons$ = this.actions$.pipe(
    ofType(personActions.LOAD_PERSONS),
    switchMap(() =>
      this.personService.list().pipe(
        map(persons => new personActions.LoadPersonsSuccess(persons)),
        catchError(error => of(new personActions.LoadPersonsFail(error)))
      )
    )
  )

  //
  // CreatePerson via the service
  //
  @Effect()
  createPerson$ = this.actions$.pipe(
    ofType(personActions.CREATE_PERSON),
    map((action: personActions.CreatePerson) => action.payload),
    switchMap(fromPayload =>
      this.personService.create(fromPayload).pipe(
        map(person => new personActions.CreatePersonSuccess(person)),
        catchError(error => of(new personActions.CreatePersonFail(error)))
      )
    )
  )

  //
  // CreatePersonSuccess - navigate to the person after it has been created.
  //
  @Effect()
  createPersonSuccess$ = this.actions$.pipe(
    ofType(personActions.CREATE_PERSON_SUCCESS),
    map((action: personActions.CreatePersonSuccess) => action.payload),
    map(person => {
      return new fromRouter.Go({
        path: [fromConstants.BASE_ROUTE, person.id]
      })
    })
  )

  //
  // Update the person via the service
  //
  @Effect()
  updatePerson$ = this.actions$.pipe(
    ofType(personActions.UPDATE_PERSON),
    map((action: personActions.UpdatePerson) => action.payload),
    switchMap(fromPayload =>
      this.personService.update(fromPayload).pipe(
        map(person => new personActions.UpdatePersonSuccess(person)),
        catchError(error => of(new personActions.UpdatePersonFail(error)))
      )
    )
    // switchMap(fromPayload => this.personService.updatePerson(fromPayload)),
    // map(person => new personActions.UpdatePersonSuccess(person)),
    // catchError(error => of(new personActions.UpdatePersonFail(error)))
  )

  @Effect()
  deletePerson$ = this.actions$.pipe(
    ofType(personActions.DELETE_PERSON),
    map((action: personActions.DeletePerson) => action.payload),
    switchMap(person =>
      this.personService.delete(person.id).pipe(
        // personService.remove returns nothing, so we return
        // the deleted person ourselves on success
        map(() => new personActions.DeletePersonSuccess(person)),
        catchError(error => of(new personActions.DeletePersonFail(error)))
      )
    )
  )

  //
  // CreatePersonSuccess - navigate to the person after it has been created.
  //
  @Effect()
  handlePersonSuccess$ = this.actions$.pipe(
    ofType(personActions.UPDATE_PERSON_SUCCESS, personActions.DELETE_PERSON_SUCCESS),
    map(() => {
      return new fromRouter.Go({
        path: [fromConstants.BASE_ROUTE]
      })
    })
  )
}
