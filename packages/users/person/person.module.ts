import { NgModule } from '@angular/core'
import { HttpModule } from '@angular/http'
import { ModuleWithProviders } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { QuillModule } from 'ngx-quill'
import { NgxDatatableModule } from '@swimlane/ngx-datatable'
import { StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'

import { PersonRoutingModule } from './person.routing'
import { reducers, effects } from './store'
import * as fromComponents from './components'
import * as fromContainers from './containers'
import * as fromGuards from './guards'
import * as fromServices from './services'

import { CoreModule, IsLoggedInGuard } from '@inzicht/core'
import { UIModule } from '@inzicht/ui'

// import { EndqualificationModule } from '../endqualification/endqualification.module'

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    CoreModule,
    UIModule,
    // EndqualificationModule,
    QuillModule,
    NgbModule,
    // Lazyload all store parts
    StoreModule.forFeature('person', reducers),
    EffectsModule.forFeature(effects),
    PersonRoutingModule
  ],
  declarations: [...fromContainers.containers, ...fromComponents.components],
  providers: [...fromServices.services, ...fromGuards.guards, IsLoggedInGuard],
  exports: [...fromContainers.containers, ...fromComponents.components]
})
export class PersonModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: PersonModule,
      providers: [...fromServices.services]
    }
  }
}
