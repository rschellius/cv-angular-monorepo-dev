import { Injectable } from '@angular/core'
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router'
import { Store } from '@ngrx/store'
import { Observable } from 'rxjs'
import { tap, filter, take, switchMap, map, flatMap } from 'rxjs/operators'

import * as fromStore from '../store'
import * as fromRouter from '@inzicht/core/router'
import { Person } from '../models'

@Injectable()
export class PersonExistsGuard implements CanActivate {
  constructor(private store: Store<fromStore.PersonState>) {}

  /**
   * Check wether the item that we navigated to exists.
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.checkStore().pipe(
      switchMap(() => this.store.select(fromRouter.getRouterState)),
      flatMap(router => router.state.params.filter(param => param['personId'])),
      switchMap((param: any) => this.hasPerson(+param.personId))
    )
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(route, state)
  }

  hasPerson(id: number): Observable<boolean> {
    return this.store.select(fromStore.getPersonsEntities).pipe(
      // Get the entity with given id, and convert to boolean
      // true or false indicates wether the item exists.
      map((entities: { [key: number]: Person }) => !!entities[id]),
      tap(result => console.log(`hasPerson ${id} = ${result}`)),
      take(1)
    )
  }

  checkStore(): Observable<boolean> {
    return this.store.select(fromStore.getPersonsLoaded).pipe(
      tap(loaded => {
        if (!loaded) {
          // Not loaded, so reload from the store
          this.store.dispatch(new fromStore.LoadPersons())
        }
      }),
      // this filter construct waits for loaded to become true
      filter(loaded => loaded),
      // this take completes the observable and unsubscribes
      take(1)
    )
  }
}
