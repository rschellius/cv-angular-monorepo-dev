import { PersonComponent } from './person.component'
import { PersonDetailComponent } from './person-detail/person-detail.component'
import { PersonListComponent } from './person-list/person-list.component'
import { PersonEditComponent } from './person-edit/person-edit.component'

export const containers: any[] = [
  PersonComponent,
  PersonDetailComponent,
  PersonListComponent,
  PersonEditComponent
]

export * from './person.component'
export * from './person-detail/person-detail.component'
export * from './person-edit/person-edit.component'
export * from './person-list/person-list.component'
