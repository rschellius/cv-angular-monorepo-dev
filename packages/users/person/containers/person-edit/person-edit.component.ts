import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { ActionsGroup } from '@inzicht/ui'
import { Store } from '@ngrx/store'
import { Observable } from 'rxjs'
import { tap } from 'rxjs/operators'

import * as fromModels from '../../models'
// import * as fromEndqualifications from '@inzicht-endqualification/models'
import * as fromStore from '../../store'

@Component({
  selector: 'cv-person-edit',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './person-edit.component.html',
  styleUrls: []
})
export class PersonEditComponent implements OnInit {
  title: string

  // Item to be edited
  person$: Observable<fromModels.Person>

  // Endqualifications of this person
  // Edited in separate component and merged into new result.
  // endqualifications: fromEndqualifications.Qualification[]

  // Option to select element as learning line.
  isLearningLine: boolean

  // Demo: Hardcoded docentnamen; zouden van de API moeten komen.
  persons = [
    { name: 'Ruud Hermans' },
    { name: 'Pascal van Gastel' },
    { name: 'Jan Montizaan' },
    { name: 'Gitta de Vaan' },
    { name: 'Eefje Gijzen' },
    { name: 'Erco Argante' },
    { name: 'Arno Broeders' },
    { name: 'Robin Schellius' },
    { name: 'Johan Smarius' },
    { name: 'Peter Vos' },
    { name: 'Heidie Tops' },
    { name: 'Ger Oosting' },
    { name: 'Evert Jan de Voogt' },
    { name: 'Gerard Wagenaar' }
  ]

  /**
   * Actions op deze page
   */
  actions: ActionsGroup[] = [
    new ActionsGroup({
      title: 'Acties',
      actions: [
        {
          name: 'Annuleren',
          routerLink: '..'
        }
      ]
    })
  ]

  constructor(
    private readonly store: Store<fromStore.PersonState>,
    private readonly route: ActivatedRoute,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.title = this.route.snapshot.data['title'] || 'Nieuwe Periode'
    this.isLearningLine = this.route.snapshot.data['optionalLearningline'] || false

    this.person$ = this.store.select(fromStore.getSelectedPerson).pipe(
      tap((person: fromModels.Person = null) => {
        const personExists = !!person
      })
    )
  }

  onCreate(event: fromModels.Person) {
    this.store.dispatch(new fromStore.CreatePerson(event))
  }

  onUpdate(event: fromModels.Person) {
    this.store.dispatch(new fromStore.UpdatePerson(event))
  }

  onRemove(event: fromModels.Person) {
    const remove = window.confirm('Are you sure?')
    if (remove) {
      this.store.dispatch(new fromStore.DeletePerson(event))
    }
  }

  onCancel() {
    this.router.navigate(['..'], { relativeTo: this.route })
  }
}
