import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { Observable, Subscription } from 'rxjs'
// import { NGXLogger } from 'ngx-logger'

import { ActionsGroup } from '@inzicht/ui'
import { Person } from '../../models'
// import { PersonComponent } from '..'

import * as fromStore from '../../store'
import * as fromConstants from '../../person.constants'
// import * as fromAuth from '@inzicht/core'
import { getIsAuthenticated } from '@inzicht/core/src/store'
import { Store } from '@ngrx/store'

@Component({
  selector: 'cv-person-detail',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './person-detail.component.html'
})
export class PersonDetailComponent implements OnInit, OnDestroy {
  // Logging classname tag
  readonly TAG = PersonDetailComponent.name

  // Modal identifier.
  public readonly DIALOG_ADD_SUBNODE = 'DialogAddSubnode'

  // Page title
  title: string

  // The core element of this Component
  // person$: Observable<Person>
  person: Person

  // User authentication role
  userMayEdit$: Observable<boolean>

  // Subscription on observable
  subscription: Subscription

  /**
   * Actions on this page
   */
  actions: ActionsGroup[] = [
    new ActionsGroup({
      title: 'Acties',
      actions: [
        {
          name: 'Toevoegen',
          routerLink: 'new'
        },
        {
          name: 'Verwijderen',
          routerLink: 'todo'
        }
      ]
    })
  ]

  constructor(
    private readonly store: Store<fromStore.PersonState>,
    private readonly router: Router,
    private readonly route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.userMayEdit$ = this.store.select(getIsAuthenticated)

    this.subscription = this.store.select(fromStore.getSelectedPerson).subscribe((result: Person) => {
      console.dir('Selected node is', result && result.id)
      this.person = result
    })
    // .pipe(
    //   tap((pizza: Person = null) => {
    //     const pizzaExists = !!(pizza && pizza.)
    //     const toppings = pizzaExists
    //       ? pizza.toppings.map(topping => topping.id)
    //       : []
    //     this.store.dispatch(new fromStore.VisualizeToppings(toppings))
    //   })
    // )

    // this.toppings$ = this.store.select(fromStore.getAllToppings)
    // this.visualise$ = this.store.select(fromStore.getPizzaVisualized)
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe()
  }

  onEdit(id: number) {
    this.router.navigate([fromConstants.BASE_ROUTE, id, 'edit'])
  }
}
