import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { HttpParams } from "@angular/common/http";

import { UsersService } from "../../services/user.service";
import { User } from "../../models/user.model";

@Component({
	selector: "cv-user-list",
	templateUrl: "./user-list.component.html",
	styles: []
})
export class UserListComponent implements OnInit {
	users: User[];

	constructor(
		private readonly userService: UsersService,
		private readonly router: Router,
		private readonly route: ActivatedRoute
	) {}

	ngOnInit() {
		this.userService.list(new HttpParams()).subscribe(users => {
			console.dir(users);
			this.users = users.map(item => new User(item));
		});
	}

	onNewUser() {
		console.error("Not implemented yet!");
	}

	gotoUserDetails(id: number) {
		this.router.navigate([id], { relativeTo: this.route });
	}
}
