import { UsersDashboardComponent } from './user-dashboard/user-dashboard.component'
import { UserDetailComponent } from './user-detail/user-detail.component'
import { UserListComponent } from './user-list/user-list.component'
import { UserItemComponent } from './user-list/user-item/user-item.component'
import { UsersComponent } from './user.component'

export const containers: any[] = [
  UsersDashboardComponent,
  UserDetailComponent,
  UserListComponent,
  UserItemComponent,
  UsersComponent
]

export * from './user.component'
export * from './user-dashboard/user-dashboard.component'
export * from './user-detail/user-detail.component'
export * from './user-list/user-list.component'
export * from './user-list/user-item/user-item.component'
