import { Component, OnInit } from "@angular/core";
import { HttpParams } from "@angular/common/http";
import { UsersService } from "../../services/user.service";
import { User } from "../../models/user.model";

@Component({
	selector: "cv-users-dashboard",
	templateUrl: "./user-dashboard.component.html",
	styles: []
})
export class UsersDashboardComponent implements OnInit {
	users: User[];
	title = "Users Dashboard";

	constructor(private readonly userService: UsersService) {}

	ngOnInit() {
		this.userService
			.list(new HttpParams())
			.subscribe(users => (this.users = users));
	}
}
