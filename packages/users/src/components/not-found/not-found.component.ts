import { Component } from '@angular/core'

@Component({
  selector: 'cv-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: []
})
export class NotFoundComponent {}
