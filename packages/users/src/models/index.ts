import { User, UserRole, UserRoleNamePipe } from './user.model'
import { StorageUser } from './user-storage.model'

export const models: any[] = [User, UserRole, UserRoleNamePipe, StorageUser]

export * from './user.model'
export * from './user-storage.model'
