import { Pipe, PipeTransform } from '@angular/core'
import { map, reduce, tap } from 'rxjs/operators'
import { Observable, from } from 'rxjs'

import { Entity } from '@inzicht/core'

/**
 * User roles.
 */
export enum UserRole {
  Basic = 0, // Heeft beperkt toegang
  Viewer = 1, // Kan alle entiteiten raadplegen
  Editor = 2, // Kan raadplegen en wijzigen
  Admin = 3, // Kan raadplegen, wijzigen en niet-actief maken
  SuperUser = 4 // Kan users toevoegen
}

/**
 * String translations of roles
 */
const userRoleNames = ['Basic', 'Viewer', 'Editor', 'Admin', 'SuperUser']

/**
 * String translation, translates nr to rolename.
 */
@Pipe({ name: 'asRole' })
export class UserRoleNamePipe implements PipeTransform {
  transform(roleNr: number) {
    return userRoleNames[roleNr]
  }
}

export interface UserInterface {
  data: {
    firstname: string
    lastname: string
    email: string
  }
  fullName: string
}

export class User extends Entity implements UserInterface {
  data!: {
    firstname: string
    lastname: string
    email: string
  }
  roles!: UserRole[]
  token!: string

  constructor(values: any = {}) {
    super(values)
    try {
      this.data = {
        firstname: values.data['firstname'],
        lastname: values.data['lastname'],
        email: values.data['email']
      }
      this.token = values.token
      this.roles = []
      if (values && values.roles && values.roles.length > 0) {
        values.roles.forEach((role: UserRole) => this.roles.push(role))
      }
    } catch (e) {
      console.error(e.toString())
    }
  }

  public hasRole(role: UserRole): Observable<boolean> {
    return from(this.roles).pipe(
      map(val => role <= val),
      reduce((a, b) => a || b),
      tap(val => console.log(`hasRole ${userRoleNames[role]} = ${val}`))
    )
  }

  get fullName(): string {
    const fullname = this.data.firstname /*.trim().charAt(0) */ + ' ' + this.data.lastname
    return fullname
  }
}
