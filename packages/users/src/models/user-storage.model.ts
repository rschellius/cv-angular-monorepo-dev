export interface StorageUserInterface {
  readonly token: string | null
  readonly userName: string | null
  readonly userRoles: number[]
}

export class StorageUser implements StorageUserInterface {
  constructor(public token: string | null, public userName: string | null, public userRoles: number[]) {}
}
