import { UserEffects } from './users.effect'

export const effects: any[] = [UserEffects]

export * from './users.effect'
