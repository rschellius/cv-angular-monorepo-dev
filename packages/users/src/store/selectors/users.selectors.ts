import { createSelector } from '@ngrx/store'

import * as fromRouter from '@inzicht/core/router'
import * as fromFeature from '../reducers'
import * as fromUsers from '../reducers/users.reducers'

import { User } from '../../models'

//
// Selectors - zijn nodig om door delen van de state tree te navigeren
// They return slices of the state tree.
//

export const getUserState = createSelector(
  fromFeature.getUsers,
  (state: fromFeature.UserState) => state.user
)

export const getUsersEntities = createSelector(
  getUserState,
  fromFeature.getUsers
)

// Get a selected pizza based on pizzaId from the route
export const getSelectedUser = createSelector(
  getUsersEntities,
  fromRouter.getRouterState,
  (entities, router): User => {
    return router.state && entities[router.state.params.pizzaId]
  }
)

export const getAllUsers = createSelector(
  getUsersEntities,
  entities => {
    return Object.keys(entities).map(id => entities[parseInt(id, 10)])
  }
)

export const getUsersLoading = createSelector(
  getUserState,
  fromUsers.getUsersLoading
)

export const getUsersLoaded = createSelector(
  getUserState,
  fromUsers.getUsersLoaded
)
