import { NgModule } from "@angular/core";
import { HttpModule } from "@angular/http";
import { CommonModule } from "@angular/common";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { reducers, effects } from "./store";

import { ModuleWithProviders } from "@angular/core";
import { UsersRoutingModule } from "./users.routing";

import * as fromComponents from "./components";
import * as fromContainers from "./containers";
import * as fromGuards from "./guards";
import * as fromServices from "./services";
import * as fromModels from "./models";
// import { CoreModule } from '@inzicht/core'

@NgModule({
	imports: [
		CommonModule,
		HttpModule,
		// CoreModule,
		UsersRoutingModule, // Order is important, MUST be BEFORE AppRoutingModule!
		// Lazyload all store parts
		StoreModule.forFeature("users", reducers),
		EffectsModule.forFeature(effects)
	],
	declarations: [
		...fromContainers.containers,
		...fromComponents.components,
		fromModels.UserRoleNamePipe
	],
	providers: [...fromServices.services, ...fromGuards.guards],
	exports: [...fromContainers.containers, ...fromComponents.components]
})
export class UsersModule {
	static forRoot(): ModuleWithProviders {
		return {
			ngModule: UsersModule,
			providers: [...fromServices.services]
		};
	}
}
