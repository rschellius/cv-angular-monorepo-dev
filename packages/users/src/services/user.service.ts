import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

import { environment } from '@inzicht/inzicht'
import { EntityService } from '@inzicht/core'

import { User } from '../models/user.model'

@Injectable()
export class UsersService extends EntityService<User> {
  constructor(http: HttpClient) {
    super(http, environment.API_BASE_URL, 'users')
  }
}
