import { DialogService } from './dialog.service'
import { UsersService } from './user.service'

export const services: any[] = [DialogService, UsersService]

export * from './dialog.service'
export * from './user.service'
