import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import {
	IsLoggedInGuard as HasLoggedIn
	//  as HasAdminRole
} from "@inzicht/core";

import * as fromComponents from "./components";
import * as fromContainers from "./containers";

const routes: Routes = [
	{
		path: "login",
		component: fromComponents.LoginComponent
	},
	{
		path: "users",
		component: fromContainers.UsersComponent,
		children: [
			{
				path: "",
				component: fromContainers.UserListComponent,
				canActivate: [HasLoggedIn] //, HasAdminRole]
			},
			{
				path: "me",
				component: fromComponents.UserProfileComponent,
				canActivate: [HasLoggedIn]
			},
			{
				path: "new",
				component: fromComponents.UserEditComponent,
				canActivate: [HasLoggedIn], //, HasAdminRole],
				data: { title: "Nieuwe gebruiker" }
			},
			{
				path: ":id",
				canActivate: [HasLoggedIn], //, HasAdminRole],
				component: fromContainers.UserDetailComponent
			},
			{
				path: ":id/edit",
				component: fromComponents.UserEditComponent,
				canActivate: [HasLoggedIn], //, HasAdminRole],
				data: { title: "Wijzig gebruiker" }
			},
			{ path: "**", component: fromComponents.UserNotFoundComponent }
		]
	}
];

@NgModule({
	imports: [
		// Always use forChild in child route modules!
		RouterModule.forChild(routes)
	],
	exports: [RouterModule]
})
export class UsersRoutingModule {}
