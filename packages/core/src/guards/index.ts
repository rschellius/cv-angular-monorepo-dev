// import { LoggedInAuthGuard, AdminRoleAuthGuard } from './auth.guards'
import { IsLoggedInGuard } from "./is-logged-in.guard";
import { CanDeactivateGuard } from "./can-deactivate.guard";

export const guards: any[] = [IsLoggedInGuard, CanDeactivateGuard];

// export * from './auth.guards'
export * from "./is-logged-in.guard";
export * from "./can-deactivate.guard";
