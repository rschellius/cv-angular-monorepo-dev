import { NgModule, ModuleWithProviders } from '@angular/core'
import { HttpModule } from '@angular/http'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'

import { LoggerModule } from 'ngx-logger'
import { environment } from '@inzicht/inzicht'

// import { AuthRoutingModule } from './auth.routing'
import * as fromComponents from './components'
import * as fromGuards from './guards'
import * as fromServices from './services'
import { reducers, effects } from './store'

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    HttpModule,
    LoggerModule.forRoot(environment.logConfig),
    // AuthRoutingModule,
    // Lazyload all store parts
    StoreModule.forFeature('auth', reducers),
    EffectsModule.forFeature(effects)
  ],
  providers: [...fromServices.services, ...fromGuards.guards],
  declarations: [...fromComponents.components],
  exports: [...fromComponents.components]
})
export class CoreModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [...fromServices.services, ...fromGuards.guards]
    }
  }
}
