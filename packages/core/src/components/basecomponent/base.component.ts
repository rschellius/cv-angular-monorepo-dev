import { Component } from '@angular/core'
import { environment } from '@inzicht/inzicht'

@Component({
  template: ``
})
export class BaseComponent {
  // Enables debug mode to display debug data, not in production
  debug = !environment.production
}
