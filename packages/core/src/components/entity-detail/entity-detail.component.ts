import { Component, OnInit, Input } from "@angular/core";

import { Entity } from "../../models/entity.model";

@Component({
	selector: "cv-entity-detail",
	templateUrl: "./entity-detail.component.html",
	styleUrls: ["./entity-detail.component.css"]
})
export class EntityDetailComponent {
	title = "Info";
	@Input() entity: Entity | undefined;

	constructor() {}
}
