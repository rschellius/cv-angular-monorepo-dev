import { Action } from '@ngrx/store'

export enum AuthActionTypes {
  IS_LOGGEDIN = '[Auth] Is Loggedin',
  IS_LOGGEDIN_SUCCESS = '[Auth] Is Loggedin Success',
  IS_LOGGEDIN_FAIL = '[Auth] Is Loggedin Failed',
  LOGIN = '[Auth] Login',
  LOGIN_SUCCESS = '[Auth] Login Success',
  LOGIN_FAIL = '[Auth] Login Failed',
  LOGOUT = '[Auth] Logout',
  LOGOUT_SUCCESS = '[Auth] Logout Success',
  LOGOUT_FAILED = '[Auth] Logout Failed'
}

export class IsLoggedIn implements Action {
  readonly type = AuthActionTypes.IS_LOGGEDIN
}

export class IsLoggedInSuccess implements Action {
  readonly type = AuthActionTypes.IS_LOGGEDIN_SUCCESS
  constructor(public payload: any) {}
}

export class IsLoggedInFail implements Action {
  readonly type = AuthActionTypes.IS_LOGGEDIN_FAIL
  constructor(public payload: any) {}
}

export class LogIn implements Action {
  readonly type = AuthActionTypes.LOGIN
  constructor(public payload: any) {}
}

export class LogInSuccess implements Action {
  readonly type = AuthActionTypes.LOGIN_SUCCESS
  constructor(public payload: any) {}
}

export class LogInFail implements Action {
  readonly type = AuthActionTypes.LOGIN_FAIL
  constructor(public payload: any) {}
}

export class LogOut implements Action {
  readonly type = AuthActionTypes.LOGOUT
}

export class LogOutSuccess implements Action {
  readonly type = AuthActionTypes.LOGOUT_SUCCESS
}

export class LogOutFailed implements Action {
  readonly type = AuthActionTypes.LOGOUT_FAILED
}

export type All =
  | LogIn
  | LogInSuccess
  | LogInFail
  | LogOut
  | LogOutSuccess
  | LogOutFailed
  | IsLoggedIn
  | IsLoggedInFail
  | IsLoggedInSuccess
