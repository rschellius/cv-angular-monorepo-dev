import { AuthEffects } from './auth.effects'
import { AlertEffects } from './alert.effects'

export const effects: any[] = [AuthEffects, AlertEffects]

export * from './auth.effects'
export * from './alert.effects'
