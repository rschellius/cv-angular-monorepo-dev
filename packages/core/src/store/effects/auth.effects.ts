import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { Actions, Effect, ofType } from '@ngrx/effects'
import { Observable, of } from 'rxjs'
import { tap, map, catchError, switchMap } from 'rxjs/operators'

import { AuthService } from '../../services'

import * as fromActions from '../actions/auth.actions'

@Injectable()
export class AuthEffects {
  constructor(private actions: Actions, private authService: AuthService, private router: Router) {}

  @Effect()
  isLoggedIn$ = this.actions.pipe(
    // tap(console.log),
    ofType(fromActions.AuthActionTypes.IS_LOGGEDIN),
    // Do we already have user data in local storage?
    switchMap(() => this.authService.getUserFromLocalStorage()),
    tap(console.log),
    // If yes, that is our logged in user
    map(user => new fromActions.IsLoggedInSuccess(user)),
    catchError(error => of(new fromActions.IsLoggedInFail(error)))
  )

  @Effect({ dispatch: false })
  LoggedInSuccess: Observable<any> = this.actions.pipe(
    ofType(fromActions.AuthActionTypes.IS_LOGGEDIN_SUCCESS),
    map((action: fromActions.IsLoggedInSuccess) => action.payload)
  )

  @Effect()
  LogIn: Observable<any> = this.actions.pipe(
    ofType(fromActions.AuthActionTypes.LOGIN),
    map((action: fromActions.LogIn) => action.payload),
    switchMap(payload => {
      return this.authService.login(payload.email, payload.password).pipe(
        map(user => {
          //
          // LET OP: connecten met echte api, dit is een DUMMY USER!
          //
          return new fromActions.LogInSuccess({
            token: user.token || 'oijasdfkajsdflkjasdfoiajeo[ij',
            userName: payload.userName || 'Robin Schellius'
          })
        }),
        catchError(error => {
          return of(new fromActions.LogInFail({ error: error }))
        })
      )
    })
  )

  @Effect({ dispatch: false })
  LogInSuccess: Observable<any> = this.actions.pipe(
    ofType(fromActions.AuthActionTypes.LOGIN_SUCCESS),
    map((action: fromActions.LogInSuccess) => action.payload),
    switchMap(user => this.authService.saveUserToLocalStorage(user)),
    tap(() => this.router.navigate(['/']))
  )

  @Effect({ dispatch: false })
  LogInFailure: Observable<any> = this.actions.pipe(ofType(fromActions.AuthActionTypes.LOGIN_FAIL))

  @Effect()
  LogOut: Observable<any> = this.actions.pipe(
    ofType(fromActions.AuthActionTypes.LOGOUT),
    switchMap(() => this.authService.logout()),
    map(() => new fromActions.LogOutSuccess()),
    catchError(error => of(new fromActions.LogOutFailed()))
  )

  @Effect({ dispatch: false })
  LogOutSuccess: Observable<any> = this.actions.pipe(
    ofType(fromActions.AuthActionTypes.LOGOUT_SUCCESS, fromActions.AuthActionTypes.LOGOUT_FAILED),
    tap(() => this.router.navigate(['/']))
  )
}
