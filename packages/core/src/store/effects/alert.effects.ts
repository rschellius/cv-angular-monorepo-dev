import { Injectable } from '@angular/core'
import { Effect, Actions, ofType } from '@ngrx/effects'
import * as AlertActions from '../actions/alert.actions'
import { map, tap } from 'rxjs/operators'
import { AlertInfo } from '../../models/alert.model'
import { ToastrService } from 'ngx-toastr'

@Injectable()
export class AlertEffects {
  constructor(private actions$: Actions, private toastr: ToastrService) {}

  //
  // We handelen hier alleen een actie af. we dispatchen daarom geen actie,
  //
  @Effect({ dispatch: false })
  success$ = this.actions$.pipe(
    ofType(AlertActions.SUCCESS),
    map((action: AlertActions.AlertSuccess) => action.payload),
    tap((alertInfo: AlertInfo) => {
      this.toastr.success(alertInfo.message, alertInfo.title)
    })
  )

  @Effect({ dispatch: false })
  warning$ = this.actions$.pipe(
    ofType(AlertActions.WARNING),
    map((action: AlertActions.AlertWarn) => action.payload),
    tap((alertInfo: AlertInfo) => {
      this.toastr.warning(alertInfo.message, alertInfo.title, {
        tapToDismiss: true,
        timeOut: 5000,
        closeButton: true
      })
    })
  )

  @Effect({ dispatch: false })
  error$ = this.actions$.pipe(
    ofType(AlertActions.ERROR),
    map((action: AlertActions.AlertError) => action.payload),
    tap((alertInfo: AlertInfo) => {
      this.toastr.error(alertInfo.message, alertInfo.title, {
        disableTimeOut: true,
        tapToDismiss: true,
        closeButton: true
      })
    })
  )
}
