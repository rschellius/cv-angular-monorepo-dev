import { createSelector } from '@ngrx/store'

// import * as fromCore from '@inzicht/core/src/store'
import * as fromFeature from '../reducers'
import * as fromAuth from '../reducers/auth.reducers'

//
// Selectors - zijn nodig om door delen van de state tree te navigeren
// They return slices of the state tree.
//

export const getAuthInfo = createSelector(
  fromFeature.getAuthState,
  (state: fromFeature.AuthState) => state.user
)

export const getAuthEntity = createSelector(
  getAuthInfo,
  fromFeature.getAuthState
)

// export const getSelectedAuth = createSelector(
//   getAuthEntity,
//   fromRoot.getRouterState,
//   (entities, router): any => {
//     return router.state && entities[router.state.params.pizzaId]
//   }
// )

// export const getAllAuth = createSelector(
//   getAuthEntities,
//   entities => {
//     return Object.keys(entities).map(id => entities[parseInt(id, 10)])
//   }
// )

export const getAuthLoading = createSelector(
  getAuthInfo,
  fromAuth.getAuthLoading
)

export const getAuthLoaded = createSelector(
  getAuthInfo,
  fromAuth.getAuthLoaded
)

export const getIsAuthenticated = createSelector(
  getAuthInfo,
  fromAuth.getIsAuthenticated
)

export const getUserName = createSelector(
  getAuthInfo,
  fromAuth.getAuthUserName
)
