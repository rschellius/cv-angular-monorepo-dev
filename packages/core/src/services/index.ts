import { AuthService } from './auth.service'
import { EntityService } from './entity.service'

export const services: any[] = [AuthService, EntityService]

export * from './auth.service'
export * from './entity.service'
