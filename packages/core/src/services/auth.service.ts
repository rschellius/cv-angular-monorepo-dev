import { Injectable } from '@angular/core'
import { Observable, of, EMPTY, throwError } from 'rxjs'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { tap, catchError } from 'rxjs/operators'
import { NGXLogger } from 'ngx-logger'

import { EntityService } from './entity.service'
import { environment } from '@inzicht/inzicht'
import { User, UserRole, StorageUser } from '@inzicht/users'

@Injectable({
  providedIn: 'root'
})
export class AuthService extends EntityService<User> {
  // Logging classname tag
  static readonly TAG = AuthService.name

  // private readonly API_LOGIN = environment.API_BASE_URL + 'users'
  private readonly API_LOGIN = environment.apiLoginUserUrl

  // store the URL so we can redirect after logging in
  public readonly redirectUrl = '/dashboard'

  private readonly headers = new HttpHeaders({
    'Content-Type': 'application/json'
  })

  constructor(private readonly logger: NGXLogger, http: HttpClient) {
    super(http, environment.API_BASE_URL, 'users')
  }

  login(email: string, password: string) {
    this.logger.debug(AuthService.TAG, 'login')
    return this.http.post(this.API_LOGIN, { email, password }, { headers: this.headers }).pipe(
      tap(console.log),
      catchError(this.handleError)
    )
  }

  public logout(): Observable<boolean> {
    this.logger.debug(AuthService.TAG, 'logout - remove local user')
    localStorage.removeItem('userName')
    localStorage.removeItem('userRoles')
    localStorage.removeItem('token')
    return of(true)
  }

  public getUserFromLocalStorage(): Observable<StorageUser> {
    console.log('getuser from storage')
    const token = localStorage.getItem('token')
    if (!token || token === null) {
      return throwError({ message: 'No user in local storage' })
    }

    const userName = localStorage.getItem('userName')
    const userRoles = localStorage.getItem('userRoles')

    const storedUser: StorageUser = {
      userName,
      userRoles: [],
      token
    }

    return of(storedUser)
  }

  public saveUserToLocalStorage(user: StorageUser): Observable<boolean> {
    localStorage.setItem('userName', 'TESTSTRING!') // user.userName)
    // localStorage.setItem('userRoles', user.userRoles)
    localStorage.setItem('token', 'TESTSTRING!') // user.token)
    return of(true)
  }
}
