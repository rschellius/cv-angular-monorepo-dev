import { ContextEffects } from './context.effect'

export const effects: any[] = [ContextEffects]

export * from './context.effect'
