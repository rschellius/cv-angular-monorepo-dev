import { Injectable } from '@angular/core'
import { Effect, Actions, ofType } from '@ngrx/effects'
import { map, switchMap } from 'rxjs/operators'

// import * as objectiveActions from '@inzicht-objective/store/actions'
// import * as boksActions from '@inzicht-boks/store/actions'
// import * as courseActions from '@inzicht-course/store/actions'
// import * as moduleActions from '@inzicht-modules/store/actions'

import { Context } from '../../models/context.model'
import * as ContextActions from '../actions/context.actions'

@Injectable()
export class ContextEffects {
  constructor(private actions$: Actions) {}

  @Effect()
  loadContext$ = this.actions$.pipe(
    ofType(ContextActions.LOAD_CONTEXT),
    map(() => new ContextActions.LoadContextSuccess(new Context(2018, '2018-2019')))
  )

  @Effect()
  changeYear$ = this.actions$.pipe(
    ofType(ContextActions.CHANGE_YEAR),
    map((action: ContextActions.ChangeYear) => action.payload),
    switchMap(fromPayload => [
      new ContextActions.ChangeYearSuccess(new Context(fromPayload.year))
      // new objectiveActions.ResetObjectives(),
      // new boksActions.ResetBoksNodes(),
      // new courseActions.ResetCourses(),
      // new moduleActions.ResetModules()
    ])
  )
}
