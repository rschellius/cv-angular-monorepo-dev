import * as fromContext from '@ngrx/router-store'
import { createSelector } from '@ngrx/store'
import * as fromFeature from '../reducers'

//
// Selectors - zijn nodig om door delen van de state tree te navigeren
// They return slices of the state tree.
//
export const getContext = createSelector(
  fromFeature.getContextState,
  (state: fromFeature.ContextState) => state
)

export const getContextYearState = createSelector(
  getContext,
  (state: fromFeature.ContextState) => state.year
)

export const getYear = createSelector(
  getContext,
  getContextYearState,
  (state: fromFeature.ContextState) => state.year.year
)

export const getFullYear = createSelector(
  getContext,
  getContextYearState,
  (state: fromFeature.ContextState) => state.year.fullYear
)
