/**
 * Het (studie)jaar waarin een curriculum geldig is.
 * year: bv. 2018
 * fullYear: bv '2018-2019'
 */
export class Context {
  constructor(public year: number, public fullYear?: string) {}
}
