import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'

import * as fromComponents from './components'
// import * as fromGuards from './guards'
// import * as fromServices from './services'
import { reducers, effects } from './store'

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    // Lazyload all store parts
    StoreModule.forFeature('context', reducers),
    EffectsModule.forFeature(effects)
  ],
  // providers: [...fromServices.services, ...fromGuards.guards],
  declarations: [...fromComponents.components],
  exports: [...fromComponents.components]
})
export class ContextModule {
  // static forRoot(): ModuleWithProviders {
  //   return {
  //     ngModule: AuthModule,
  //     providers: [...fromServices.services, ...fromGuards.guards]
  //   }
  // }
}
