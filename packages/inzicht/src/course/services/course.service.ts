import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

import { EntityService } from '@inzicht/core'
import { environment } from '@inzicht/inzicht'

import { Course } from '../models/course.model'

@Injectable()
export class CourseService extends EntityService<Course> {
  constructor(httpClient: HttpClient) {
    super(httpClient, environment.API_BASE_URL, 'courses')
  }
}
