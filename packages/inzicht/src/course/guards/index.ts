import { CourseGuard } from './course.guard'
import { CourseExistsGuard } from './course-exists.guard'

export const guards: any[] = [CourseGuard, CourseExistsGuard]

export * from './course.guard'
export * from './course-exists.guard'
