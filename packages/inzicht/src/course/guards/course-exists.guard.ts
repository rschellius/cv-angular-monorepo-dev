import { Injectable } from '@angular/core'
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router'
import { Store } from '@ngrx/store'
import { Observable } from 'rxjs'
import { tap, filter, take, switchMap, map, flatMap } from 'rxjs/operators'

import * as fromStore from '../store'
import * as fromRouter from '@inzicht/core/router'
import { Course } from '../models'

@Injectable()
export class CourseExistsGuard implements CanActivate {
  constructor(private store: Store<fromStore.CourseState>) {}

  /**
   * Check wether the item that we navigated to exists.
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.checkStore().pipe(
      switchMap(() => this.store.select(fromRouter.getRouterState)),
      tap(router => console.log('in course-exists.guard.ts: router =', router)),
      flatMap(router => router.state.params.filter(param => param['courseId'])),
      switchMap((param: any) => this.hasCourse(+param.courseId))
    )
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(route, state)
  }

  hasCourse(id: number): Observable<boolean> {
    return this.store.select(fromStore.getCoursesEntities).pipe(
      // Get the entity with given id, and convert to boolean
      // true or false indicates wether the item exists.
      map((entities: { [key: number]: Course }) => !!entities[id]),
      tap(result => console.log(`hasCourse ${id} = ${result}`)),
      take(1)
    )
  }

  checkStore(): Observable<boolean> {
    return this.store.select(fromStore.getCoursesLoaded).pipe(
      tap(loaded => {
        if (!loaded) {
          // Not loaded, so reload from the store
          this.store.dispatch(new fromStore.LoadCourses())
        }
      }),
      // this filter construct waits for loaded to become true
      filter(loaded => loaded),
      // this take completes the observable and unsubscribes
      take(1)
    )
  }
}
