import { Component, Output, EventEmitter, OnInit, ChangeDetectionStrategy, Input } from '@angular/core'
import { Store } from '@ngrx/store'

import { Course } from '../../models'
import * as fromStore from '../../store'
import { Observable } from 'rxjs'

@Component({
  selector: 'cv-course-selector',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './course-selector.component.html'
})
export class CourseSelectorComponent implements OnInit {
  @Input() currentCourseId: number
  @Output() selected = new EventEmitter<number>()

  availableCourses$: Observable<Course[]>
  selectedCourse: number

  constructor(private readonly store: Store<fromStore.CourseState>) {}

  ngOnInit(): void {
    this.availableCourses$ = this.store.select(fromStore.getAllCourses)
    if (this.currentCourseId) {
      this.selectedCourse = this.currentCourseId
    }
  }

  onSelectionChanged() {
    this.selected.emit(this.selectedCourse)
  }
}
