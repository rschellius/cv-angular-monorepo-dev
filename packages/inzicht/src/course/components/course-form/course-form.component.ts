import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  OnChanges,
  SimpleChanges,
  ChangeDetectionStrategy
} from '@angular/core'
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'

import { Person } from '@inzicht/users'
import { Course } from '../../models/course.model'

@Component({
  selector: 'cv-course-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './course-form.component.html'
})
export class CourseFormComponent implements OnInit, OnChanges {
  exists = false

  @Input() course: Course

  @Output() selected = new EventEmitter<Course>()
  @Output() create = new EventEmitter<Course>()
  @Output() update = new EventEmitter<Course>()
  @Output() remove = new EventEmitter<Course>()
  @Output() cancel = new EventEmitter<void>()

  form = this.fb.group({
    name: ['', Validators.required],
    shortDescription: ['', Validators.required],
    longDescription: [''],
    administrativeRemark: [''],
    studyYear: ['', Validators.required],
    trimester: ['', Validators.required],
    form: ['', Validators.required],
    modules: [[]],
    responsiblePersonId: ['', Validators.required]
  })

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {}

  get nameControl() {
    return this.form.get('name') as FormControl
  }

  get nameControlInvalid() {
    return this.nameControl.hasError('required') && this.nameControl.touched
  }

  get shortDescriptionControl() {
    return this.form.get('shortDescription') as FormControl
  }

  get shortDescriptionControlInvalid() {
    return this.shortDescriptionControl.hasError('required') && this.shortDescriptionControl.touched
  }

  get longDescriptionControl() {
    return this.form.get('longDescription') as FormControl
  }

  get administrativeRemark() {
    return this.form.get('administrativeRemark') as FormControl
  }

  get learningLineControl() {
    return this.form.get('learningLine') as FormControl
  }

  get moduleControl() {
    return this.form.get('module') as FormControl
  }

  get studyYearControl() {
    return this.form.get('studyYear') as FormControl
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.course && this.course.id) {
      this.exists = true
      this.form.patchValue(this.course)
    }
  }

  createItem(form: FormGroup) {
    console.log('createItem')
    const { value, valid } = form
    if (valid) {
      this.create.emit(value)
    }
  }

  updateItem(form: FormGroup) {
    const { value, valid, touched } = form
    // if (touched && valid) {
    if (valid) {
      this.update.emit({ ...this.course, ...value })
    }
  }

  removeItem(form: FormGroup) {
    const { value } = form
    this.remove.emit({ ...this.course, ...value })
  }

  onCancel() {
    this.cancel.emit()
  }

  onPersonSelected(personId: number) {
    // console.log(person)
    this.form.controls['responsiblePersonId'].setValue(personId)
  }
}
