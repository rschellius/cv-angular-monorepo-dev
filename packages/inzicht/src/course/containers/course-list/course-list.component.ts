import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { Observable } from 'rxjs'
import { NGXLogger } from 'ngx-logger'
import { Store } from '@ngrx/store'

import { ActionsGroup } from '@inzicht/ui'

import * as fromContext from '@inzicht/core/context/store'
import * as fromAuth from '@inzicht/core/src/store'
import * as fromStore from '../../store'
import * as fromConstants from '../../course.constants'

import { Course } from '../../models'

@Component({
  selector: 'cv-course-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './course-list.component.html'
})
export class CourseListComponent implements OnInit {
  // Class name for logging
  static TAG = CourseListComponent.name

  title = 'Periodes'

  selectedYear$: Observable<string>

  // The core element of this Component
  courses$: Observable<Course[]>

  // User authentication role
  userMayEdit$: Observable<boolean>

  columnHeaders = [
    { prop: 'studyYear', name: 'Jaar', flexGrow: 1 },
    { prop: 'trimester', name: 'Periode', flexGrow: 1 },
    { prop: 'name', name: 'Naam', flexGrow: 5 },
    { prop: 'responsiblePerson.firstName', name: 'Eigenaar', flexGrow: 3 }
  ]

  selected: any = []

  /**
   * Actions op deze page
   */
  actions: ActionsGroup[] = [
    new ActionsGroup({
      title: 'Acties',
      actions: [
        {
          name: 'Element toevoegen',
          routerLink: 'new'
        },
        {
          name: 'Leerdoel overzicht',
          routerLink: 'todo'
        }
      ]
    }),
    new ActionsGroup({
      title: 'Andere acties',
      actions: [
        {
          name: 'Elementen verwijderen',
          routerLink: 'edit'
        }
      ]
    })
  ]

  constructor(
    private readonly store: Store<fromStore.CourseState>,
    private readonly logger: NGXLogger,
    private readonly router: Router,
    private readonly route: ActivatedRoute
  ) {}

  ngOnInit() {
    console.log('CourseList onInit')
    this.userMayEdit$ = this.store.select(fromAuth.getIsAuthenticated)
    this.courses$ = this.store.select(fromStore.getAllCourses)
    this.selectedYear$ = this.store.select(fromContext.getFullYear)
  }

  onAddElement() {
    this.logger.debug(CourseListComponent.TAG, 'Add element')
    this.router.navigate([fromConstants.BASE_ROUTE, 'new'])
  }

  onSelect({ selected }) {
    if (selected.length === 1) {
      const element = selected[0] as Course
      this.logger.debug(CourseListComponent.TAG, 'onSelect id = ' + element.id)
      this.router.navigate([element.id], { relativeTo: this.route })
    }
  }
}
