import { Component } from '@angular/core'

@Component({
  selector: 'cv-course',
  template: `
    <cv-course-topnav></cv-course-topnav>
    <div class="app">
      <div class="app__content">
        <div class="app__container">
          <router-outlet></router-outlet>
        </div>
      </div>
    </div>
  `
})
export class CourseComponent {}
