import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { ActionsGroup } from '@inzicht/ui'
import { Store } from '@ngrx/store'
import { Observable } from 'rxjs'
import { tap } from 'rxjs/operators'

import * as fromEndqualifications from '../../../endqualification/models'
import * as fromModels from '../../models'
import * as fromStore from '../../store'
import { DialogService } from '@inzicht/users'

@Component({
  selector: 'cv-course-edit',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './course-edit.component.html',
  styleUrls: []
})
export class CourseEditComponent implements OnInit {
  title: string

  // Item to be edited
  course$: Observable<fromModels.Course>

  /**
   * Actions op deze page
   */
  actions: ActionsGroup[] = [
    new ActionsGroup({
      title: 'Acties',
      actions: [
        {
          name: 'Annuleren',
          routerLink: '..'
        }
      ]
    })
  ]

  constructor(
    private readonly store: Store<fromStore.CourseState>,
    private readonly dialogService: DialogService,
    private readonly route: ActivatedRoute,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.title = this.route.snapshot.data['title'] || 'Nieuwe Periode'

    this.course$ = this.store.select(fromStore.getSelectedCourse).pipe(
      tap((course: fromModels.Course = null) => {
        const courseExists = !!course
      })
    )
  }

  onCreate(event: fromModels.Course) {
    this.store.dispatch(new fromStore.CreateCourse(event))
  }

  onUpdate(event: fromModels.Course) {
    this.store.dispatch(new fromStore.UpdateCourse(event))
  }

  onRemove(event: fromModels.Course) {
    const remove = window.confirm('Are you sure?')
    if (remove) {
      this.store.dispatch(new fromStore.DeleteCourse(event))
    }
  }

  onCancel() {
    this.router.navigate(['..'], { relativeTo: this.route })
  }

  canDeactivate() {
    // Allow synchronous navigation (`true`) if no crisis or the crisis is unchanged
    //   if (!this.form.dirty || this.form.untouched) {
    //     console.log('not dirty or untouched')
    //     return true
    //   }
    return this.dialogService.confirm(
      'Je hebt mogelijk wijzigingen in het formulier aangebracht. Wijzigingen weggooien?'
    )
    // }
  }
}
