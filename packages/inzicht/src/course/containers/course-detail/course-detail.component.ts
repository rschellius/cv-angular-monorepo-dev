import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { Observable, Subscription } from 'rxjs'
import { Store } from '@ngrx/store'

import { BaseComponent } from '@inzicht/core'
import { ActionsGroup } from '@inzicht/ui'
import * as fromAuth from '@inzicht/core'

import { Course } from '../../models'
import * as fromStore from '../../store'
import * as fromConstants from '../../course.constants'

@Component({
  selector: 'cv-course-detail',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './course-detail.component.html'
})
export class CourseDetailComponent extends BaseComponent implements OnInit, OnDestroy {
  // Logging classname tag
  readonly TAG = CourseDetailComponent.name

  // Modal identifier.
  public readonly DIALOG_ADD_SUBNODE = 'DialogAddSubnode'

  // Page title
  title: string

  // The core element of this Component
  // course$: Observable<Course>
  course: Course

  // User authentication role
  userMayEdit$: Observable<boolean>

  // Subscription on observable
  subscription: Subscription

  /**
   * Actions on this page
   */
  actions: ActionsGroup[] = [
    new ActionsGroup({
      title: 'Acties',
      actions: [
        {
          name: 'Toevoegen',
          routerLink: 'new'
        },
        {
          name: 'Verwijderen',
          routerLink: 'todo'
        }
      ]
    })
  ]

  constructor(
    private readonly store: Store<fromStore.CourseState>,
    private readonly router: Router,
    private readonly route: ActivatedRoute
  ) {
    super()
  }

  ngOnInit() {
    this.userMayEdit$ = this.store.select(fromAuth.getIsAuthenticated)

    this.subscription = this.store.select(fromStore.getSelectedCourse).subscribe((result: Course) => {
      console.dir('Selected node is', result && result.id)
      this.course = result
    })
    // .pipe(
    //   tap((pizza: Course = null) => {
    //     const pizzaExists = !!(pizza && pizza.)
    //     const toppings = pizzaExists
    //       ? pizza.toppings.map(topping => topping.id)
    //       : []
    //     this.store.dispatch(new fromStore.VisualizeToppings(toppings))
    //   })
    // )

    // this.toppings$ = this.store.select(fromStore.getAllToppings)
    // this.visualise$ = this.store.select(fromStore.getPizzaVisualized)
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe()
  }

  onEdit(id: number) {
    this.router.navigate([fromConstants.BASE_ROUTE, id, 'edit'])
  }
}
