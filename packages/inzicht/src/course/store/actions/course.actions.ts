import { Action } from '@ngrx/store'
import { Course } from '../../models'

export const RESET_COURSES = '[Courses] Reset Courses'

// Action creators
export class ResetCourses implements Action {
  readonly type = RESET_COURSES
}

//
// Load actions
//
export const LOAD_COURSES = '[Courses] Load Courses'
export const LOAD_COURSES_FAIL = '[Courses] Load Courses Fail'
export const LOAD_COURSES_SUCCESS = '[Courses] Load Courses Success'

// Action creators
export class LoadCourses implements Action {
  readonly type = LOAD_COURSES
}

export class LoadCoursesFail implements Action {
  readonly type = LOAD_COURSES_FAIL
  constructor(public payload: any) {}
}

export class LoadCoursesSuccess implements Action {
  readonly type = LOAD_COURSES_SUCCESS
  constructor(public payload: Course[]) {}
}

//
// Create Actions
//
export const CREATE_COURSE = '[Courses] Create Course'
export const CREATE_COURSE_FAIL = '[Courses] Create Course Fail'
export const CREATE_COURSE_SUCCESS = '[Courses] Create Course Success'

// Action creators
export class CreateCourse implements Action {
  readonly type = CREATE_COURSE
  constructor(public payload: Course) {}
}

export class CreateCourseFail implements Action {
  readonly type = CREATE_COURSE_FAIL
  constructor(public payload: any) {}
}

export class CreateCourseSuccess implements Action {
  readonly type = CREATE_COURSE_SUCCESS
  constructor(public payload: Course) {}
}

//
// Update Actions
//
export const UPDATE_COURSE = '[Courses] Update Course'
export const UPDATE_COURSE_FAIL = '[Courses] Update Course Fail'
export const UPDATE_COURSE_SUCCESS = '[Courses] Update Course Success'

// Action creators
export class UpdateCourse implements Action {
  readonly type = UPDATE_COURSE
  constructor(public payload: Course) {}
}

export class UpdateCourseFail implements Action {
  readonly type = UPDATE_COURSE_FAIL
  constructor(public payload: any) {}
}

export class UpdateCourseSuccess implements Action {
  readonly type = UPDATE_COURSE_SUCCESS
  constructor(public payload: Course) {}
}

//
// Delete Actions
//
export const DELETE_COURSE = '[Courses] Delete Course'
export const DELETE_COURSE_FAIL = '[Courses] Delete Course Fail'
export const DELETE_COURSE_SUCCESS = '[Courses] Delete Course Success'

// Action creators
export class DeleteCourse implements Action {
  readonly type = DELETE_COURSE
  constructor(public payload: Course) {}
}

export class DeleteCourseFail implements Action {
  readonly type = DELETE_COURSE_FAIL
  constructor(public payload: any) {}
}

export class DeleteCourseSuccess implements Action {
  readonly type = DELETE_COURSE_SUCCESS
  constructor(public payload: Course) {}
}

// action types
export type CoursesAction =
  | ResetCourses
  | LoadCourses
  | LoadCoursesFail
  | LoadCoursesSuccess
  | CreateCourse
  | CreateCourseFail
  | CreateCourseSuccess
  | UpdateCourse
  | UpdateCourseFail
  | UpdateCourseSuccess
  | DeleteCourse
  | DeleteCourseFail
  | DeleteCourseSuccess
