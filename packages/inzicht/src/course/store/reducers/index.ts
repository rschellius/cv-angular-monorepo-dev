import { ActionReducerMap, createFeatureSelector } from '@ngrx/store'
import * as fromCoursees from './course.reducer'

// Hier ontstaat de state tree
export interface CourseState {
  courseElements: fromCoursees.CourseState
}

// De ActionReducerMap zorgt voor typechecking. We kunnen niet zo maar functies toevoegen;
// deze moeten nu uit de Coursestate komen.
export const reducers: ActionReducerMap<CourseState> = {
  // koppel items aan de reducer function
  courseElements: fromCoursees.reducer
}

export const getCoursesState = createFeatureSelector<CourseState>('course')
