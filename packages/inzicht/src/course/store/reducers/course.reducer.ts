import * as fromCourse from '../actions'
import { Course } from '../../models'

//
// CourseState interface
//
export interface CourseState {
  entities: {
    [id: number]: Course
  }
  loaded: boolean
  loading: boolean
}

//
// Initialisation
//
export const initialState: CourseState = {
  entities: {},
  loaded: false,
  loading: false
}

//
//
//
export function reducer(state: CourseState = initialState, action: fromCourse.CoursesAction): CourseState {
  switch (action.type) {
    case fromCourse.RESET_COURSES: {
      return initialState
    }

    case fromCourse.LOAD_COURSES: {
      return {
        ...state,
        loading: true
      }
    }

    case fromCourse.LOAD_COURSES_SUCCESS: {
      const courseElements = action.payload
      const entities = courseElements.reduce(
        (
          entities: { [id: number]: Course },
          courseElement: Course
          // entities, courseElement
        ) => {
          return {
            ...entities,
            [courseElement.id]: courseElement
          }
        },
        {
          ...state.entities
        }
      )
      return {
        ...state,
        loading: false,
        loaded: true,
        entities
      }
    }

    case fromCourse.LOAD_COURSES_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: false
      }
    }

    case fromCourse.CREATE_COURSE_SUCCESS:
    case fromCourse.UPDATE_COURSE_SUCCESS: {
      const courseElement = action.payload
      const entities = {
        ...state.entities,
        [courseElement.id]: courseElement
      }
      return {
        ...state,
        entities
      }
    }

    case fromCourse.DELETE_COURSE_SUCCESS: {
      const courseElement = action.payload
      // destructure the courseElement from the state object
      // select the courseElement by id and name that 'removed'
      // the remainder are the enteties, without the removed.
      // ES6 destructuring syntax!
      const { [courseElement.id]: removed, ...entities } = state.entities

      return {
        ...state,
        entities
      }
    }

    default: {
      return state
    }
  }
}

// Selector functions: get the pieces of our state that we need
export const getCoursesEntities = (state: CourseState) => state.entities
export const getCoursesLoading = (state: CourseState) => state.loading
export const getCoursesLoaded = (state: CourseState) => state.loaded
