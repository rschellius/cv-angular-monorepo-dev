import { createSelector } from '@ngrx/store'

import * as fromRouter from '@inzicht/core/router'
import * as fromFeature from '../reducers'
import * as fromCourses from '../reducers/course.reducer'
import { Course } from '../../models'

//
// Selectors - zijn nodig om door delen van de state tree te navigeren
// They return slices of the state tree.
//
export const getCourseState = createSelector(
  fromFeature.getCoursesState,
  (state: fromFeature.CourseState) => state.courseElements
)

export const getCoursesEntities = createSelector(
  getCourseState,
  fromCourses.getCoursesEntities
)

// Get the selected item based on id from the route
export const getSelectedCourse = createSelector(
  getCoursesEntities,
  fromRouter.getRouterState,
  (entities, router): Course => {
    const params: any[] = router.state.params.filter((item: any) => item['courseId'])
    return router.state && params && params[0] && entities[params[0]['courseId']]
  }
)

export const getAllCourses = createSelector(
  getCoursesEntities,
  entities => {
    // Return an array version of our entities object
    // so that we can iterate over it via ngFor in HTML.
    return Object.keys(entities).map(id => entities[parseInt(id, 10)])
  }
)

export const getCoursesLoading = createSelector(
  getCourseState,
  fromCourses.getCoursesLoading
)

export const getCoursesLoaded = createSelector(
  getCourseState,
  fromCourses.getCoursesLoaded
)
