import { Injectable } from '@angular/core'
import { Actions, ofType, Effect } from '@ngrx/effects'
import { switchMap, map, catchError, tap } from 'rxjs/operators'
import { of } from 'rxjs'

import { AlertInfo } from '@inzicht/core'
import * as fromRouter from '@inzicht/core/router'
import * as fromCore from '@inzicht/core/src/store'
import * as fromContext from '@inzicht/core/context'
import * as courseActions from '../actions'
import * as fromServices from '../../services'
import * as fromConstants from '../../course.constants'
import { HttpParams } from '@angular/common/http'
import { Store } from '@ngrx/store'

@Injectable()
export class CourseEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<fromContext.ContextState>,
    private readonly courseService: fromServices.CourseService
  ) {}

  @Effect()
  loadCourses$ = this.actions$.pipe(
    ofType(courseActions.LOAD_COURSES),
    switchMap(() => this.store.select(fromContext.getYear)),
    switchMap(year => {
      const params = new HttpParams().set('year', '' + year)
      return this.courseService.list(params).pipe(
        map(courses => new courseActions.LoadCoursesSuccess(courses)),
        catchError(error => of(new courseActions.LoadCoursesFail(error)))
      )
    })
  )

  //
  // CreateCourse via the service
  //
  @Effect()
  createCourse$ = this.actions$.pipe(
    ofType(courseActions.CREATE_COURSE),
    map((action: courseActions.CreateCourse) => action.payload),
    switchMap(fromPayload =>
      this.courseService.create(fromPayload).pipe(
        map(course => new courseActions.CreateCourseSuccess(course)),
        catchError(error => of(new courseActions.CreateCourseFail(error)))
      )
    )
  )

  //
  // CreateCourseSuccess - navigate to the course after it has been created.
  //
  @Effect()
  createCourseSuccess$ = this.actions$.pipe(
    ofType(courseActions.CREATE_COURSE_SUCCESS),
    map((action: courseActions.CreateCourseSuccess) => action.payload),
    switchMap(course => [
      new fromCore.AlertSuccess(new AlertInfo('Success', `${course.name} aangemaakt`)),
      new fromRouter.Go({
        path: [fromConstants.BASE_ROUTE, course.id]
      })
    ])
  )

  //
  // Update the course via the service
  //
  @Effect()
  updateCourse$ = this.actions$.pipe(
    ofType(courseActions.UPDATE_COURSE),
    map((action: courseActions.UpdateCourse) => action.payload),
    switchMap(fromPayload =>
      this.courseService.update(fromPayload).pipe(
        map(course => new courseActions.UpdateCourseSuccess(course)),
        catchError(error => of(new courseActions.UpdateCourseFail(error)))
      )
    )
  )

  @Effect()
  updateSuccess$ = this.actions$.pipe(
    ofType(courseActions.UPDATE_COURSE_SUCCESS),
    map((action: courseActions.UpdateCourseSuccess) => action.payload),
    switchMap((course: any) => [
      new fromCore.AlertSuccess(new AlertInfo('Success', `${course.name} is bijgewerkt.`)),
      new fromRouter.Go({
        path: [fromConstants.BASE_ROUTE, course.id]
      })
    ])
  )

  @Effect()
  deleteCourse$ = this.actions$.pipe(
    ofType(courseActions.DELETE_COURSE),
    map((action: courseActions.DeleteCourse) => action.payload),
    switchMap(course =>
      this.courseService.delete(course.id).pipe(
        // courseService.remove returns nothing, so we return
        // the deleted course ourselves on success
        map(() => new courseActions.DeleteCourseSuccess(course)),
        catchError(error => of(new courseActions.DeleteCourseFail(error)))
      )
    )
  )

  @Effect()
  deleteSuccess$ = this.actions$.pipe(
    ofType(courseActions.DELETE_COURSE_SUCCESS),
    map((action: courseActions.DeleteCourseSuccess) => action.payload),
    switchMap(() => [
      new fromCore.AlertSuccess(new AlertInfo('Success', 'Item was deleted.')),
      new fromRouter.Go({
        path: [fromConstants.BASE_ROUTE]
      })
    ])
  )

  @Effect()
  handleFail$ = this.actions$.pipe(
    ofType(
      courseActions.LOAD_COURSES_FAIL,
      courseActions.CREATE_COURSE_FAIL,
      courseActions.UPDATE_COURSE_FAIL,
      courseActions.DELETE_COURSE_FAIL
    ),
    map((action: any) => action.payload),
    map(info => new fromCore.AlertError(new AlertInfo(info.title, info.message)))
  )
}
