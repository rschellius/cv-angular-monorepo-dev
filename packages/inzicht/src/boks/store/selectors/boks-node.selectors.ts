import { createSelector } from '@ngrx/store'

import * as fromRouter from '@inzicht/core/router'
import * as fromFeature from '../reducers'
import * as fromBoksElements from '../reducers/boks-node.reducer'
import { BoksNodeElement } from '../../models'

//
// Selectors - zijn nodig om door delen van de state tree te navigeren
// They return slices of the state tree.
//
export const getBoksElementState = createSelector(
  fromFeature.getBoksElementsState,
  (state: fromFeature.BoksElementState) => state.boksNodeElements
)

export const getBoksElementsEntities = createSelector(
  getBoksElementState,
  fromBoksElements.getBoksElementsEntities
)

// Get the selected item based on id from the route
export const getSelectedBoksElement = createSelector(
  getBoksElementsEntities,
  fromRouter.getRouterState,
  (entities, router): BoksNodeElement => {
    const params: any[] = router.state.params.filter((item: any) => item['boksId'])
    return router.state && params && params[0] && entities[params[0]['boksId']]
  }
)

// Get the selected item based on id from the route
export const getSelectedBoksElementContent = createSelector(
  getSelectedBoksElement,
  boksElement => boksElement.contentNodes
)

export const getAllBoksElements = createSelector(
  getBoksElementsEntities,
  entities => {
    // Return an array version of our entities object
    // so that we can iterate over it via ngFor in HTML.
    return Object.keys(entities).map(id => entities[parseInt(id, 10)])
  }
)

export const getToplevelElements = createSelector(
  getAllBoksElements,
  elements => elements.filter(item => item.parentId === null || item.parentId === undefined)
)

export const getBoksElementsAsTree = createSelector(
  getBoksElementsEntities,
  entities => {
    // Create an array version of our entities object
    let nodes: BoksNodeElement[] = Object.keys(entities).map(id => entities[parseInt(id, 10)])

    const makeTree = (list: BoksNodeElement[], item: BoksNodeElement): BoksNodeElement => {
      return {
        ...item,
        childNodes: item.childNodes.map(child =>
          makeTree(list, list.find(lookUpItem => lookUpItem.id === child.id))
        )
      }
    }

    return nodes
      .filter(item => item.parentId === null || item.parentId === undefined)
      .map(item => makeTree(nodes, item))
  }
)

export const getLearningLines = createSelector(
  getAllBoksElements,
  elements => elements.filter(item => item.isLearningLine)
)

export const getBoksElementsLoading = createSelector(
  getBoksElementState,
  fromBoksElements.getBoksElementsLoading
)

export const getBoksElementsLoaded = createSelector(
  getBoksElementState,
  fromBoksElements.getBoksElementsLoaded
)
