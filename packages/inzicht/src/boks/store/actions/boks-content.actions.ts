// import { Action } from '@ngrx/store'
// import { BoksContentElement } from '../../models'

// //
// // Load actions
// //
// // export const LOAD_BOKS_CONTENTS = '[BoksNodes] Load BoksContents'
// // export const LOAD_BOKS_CONTENTS_FAIL = '[BoksNodes] Load BoksContents Fail'
// // export const LOAD_BOKS_CONTENTS_SUCCESS =
// //   '[BoksNodes] Load BoksContents Success'

// // // Action creators
// // export class LoadBoksContents implements Action {
// //   readonly type = LOAD_BOKS_CONTENTS
// // }

// // export class LoadBoksContentsFail implements Action {
// //   readonly type = LOAD_BOKS_CONTENTS_FAIL
// //   constructor(public payload: any) {}
// // }

// // export class LoadBoksContentsSuccess implements Action {
// //   readonly type = LOAD_BOKS_CONTENTS_SUCCESS
// //   constructor(public payload: BoksContentElement[]) {}
// // }

// //
// // Create BoksContent Actions
// //
// export const CREATE_BOKS_CONTENT = '[BoksNodes] Create BoksContent'
// export const CREATE_BOKS_CONTENT_FAIL = '[BoksNodes] Create BoksContent Fail'
// export const CREATE_BOKS_CONTENT_SUCCESS = '[BoksNodes] Create BoksContent Success'

// // Action creators
// export class CreateBoksContent implements Action {
//   readonly type = CREATE_BOKS_CONTENT
//   constructor(public payload: BoksContentElement) {}
// }

// export class CreateBoksContentFail implements Action {
//   readonly type = CREATE_BOKS_CONTENT_FAIL
//   constructor(public payload: any) {}
// }

// export class CreateBoksContentSuccess implements Action {
//   readonly type = CREATE_BOKS_CONTENT_SUCCESS
//   constructor(public payload: BoksContentElement) {}
// }

// //
// // Update BoksContent Actions
// //
// export const UPDATE_BOKS_CONTENT = '[BoksNodes] Update BoksContent'
// export const UPDATE_BOKS_CONTENT_FAIL = '[BoksNodes] Update BoksContent Fail'
// export const UPDATE_BOKS_CONTENT_SUCCESS = '[BoksNodes] Update BoksContent Success'

// // Action creators
// export class UpdateBoksContent implements Action {
//   readonly type = UPDATE_BOKS_CONTENT
//   constructor(public payload: BoksContentElement) {}
// }

// export class UpdateBoksContentFail implements Action {
//   readonly type = UPDATE_BOKS_CONTENT_FAIL
//   constructor(public payload: any) {}
// }

// export class UpdateBoksContentSuccess implements Action {
//   readonly type = UPDATE_BOKS_CONTENT_SUCCESS
//   constructor(public payload: BoksContentElement) {}
// }

// //
// // Delete BoksContent Actions
// //
// export const DELETE_BOKS_CONTENT = '[BoksNodes] Delete BoksContent'
// export const DELETE_BOKS_CONTENT_FAIL = '[BoksNodes] Delete BoksContent Fail'
// export const DELETE_BOKS_CONTENT_SUCCESS = '[BoksNodes] Delete BoksContent Success'

// // Action creators
// export class DeleteBoksContent implements Action {
//   readonly type = DELETE_BOKS_CONTENT
//   constructor(public payload: BoksContentElement) {}
// }

// export class DeleteBoksContentFail implements Action {
//   readonly type = DELETE_BOKS_CONTENT_FAIL
//   constructor(public payload: any) {}
// }

// export class DeleteBoksContentSuccess implements Action {
//   readonly type = DELETE_BOKS_CONTENT_SUCCESS
//   constructor(public payload: BoksContentElement) {}
// }

// // action types
// export type BoksContentsAction =
//   // | LoadBoksContents
//   // | LoadBoksContentsFail
//   // | LoadBoksContentsSuccess
//   | CreateBoksContent
//   | CreateBoksContentFail
//   | CreateBoksContentSuccess
//   | UpdateBoksContent
//   | UpdateBoksContentFail
//   | UpdateBoksContentSuccess
//   | DeleteBoksContent
//   | DeleteBoksContentFail
//   | DeleteBoksContentSuccess
