import { Action } from '@ngrx/store'
import { BoksNodeElement, BoksContentElement } from '../../models'

export const RESET_BOKS_NODES = '[BoksNodes] Reset BoksNodes'

// Action creators
export class ResetBoksNodes implements Action {
  readonly type = RESET_BOKS_NODES
}

//
// Load actions
//
export const LOAD_BOKS_NODES = '[BoksNodes] Load BoksNodes'
export const LOAD_BOKS_NODES_FAIL = '[BoksNodes] Load BoksNodes Fail'
export const LOAD_BOKS_NODES_SUCCESS = '[BoksNodes] Load BoksNodes Success'

// Action creators
export class LoadBoksNodes implements Action {
  readonly type = LOAD_BOKS_NODES
}

export class LoadBoksNodesFail implements Action {
  readonly type = LOAD_BOKS_NODES_FAIL
  constructor(public payload: any) {}
}

export class LoadBoksNodesSuccess implements Action {
  readonly type = LOAD_BOKS_NODES_SUCCESS
  constructor(public payload: BoksNodeElement[]) {}
}

//
// Create BoksNode Actions
//
export const CREATE_BOKS_NODE = '[BoksNodes] Create BoksNode'
export const CREATE_BOKS_NODE_FAIL = '[BoksNodes] Create BoksNode Fail'
export const CREATE_BOKS_NODE_SUCCESS = '[BoksNodes] Create BoksNode Success'

// Action creators
export class CreateBoksNode implements Action {
  readonly type = CREATE_BOKS_NODE
  constructor(public payload: BoksNodeElement) {}
}

export class CreateBoksNodeFail implements Action {
  readonly type = CREATE_BOKS_NODE_FAIL
  constructor(public payload: any) {}
}

export class CreateBoksNodeSuccess implements Action {
  readonly type = CREATE_BOKS_NODE_SUCCESS
  constructor(public payload: BoksNodeElement) {}
}

//
// Create BoksSubNode Actions
//
export const CREATE_BOKS_SUBNODE = '[BoksNodes] Create BoksSubNode'
export const CREATE_BOKS_SUBNODE_FAIL = '[BoksNodes] Create BoksSubNode Fail'
export const CREATE_BOKS_SUBNODE_SUCCESS = '[BoksNodes] Create BoksSubNode Success'

// Action creators
export class CreateBoksSubNode implements Action {
  readonly type = CREATE_BOKS_SUBNODE
  constructor(public payload: BoksNodeElement) {}
}

export class CreateBoksSubNodeFail implements Action {
  readonly type = CREATE_BOKS_SUBNODE_FAIL
  constructor(public payload: any) {}
}

export class CreateBoksSubNodeSuccess implements Action {
  readonly type = CREATE_BOKS_SUBNODE_SUCCESS
  constructor(public payload: BoksNodeElement) {}
}

//
// Update BoksNode Actions
//
export const UPDATE_BOKS_NODE = '[BoksNodes] Update BoksNode'
export const UPDATE_BOKS_NODE_FAIL = '[BoksNodes] Update BoksNode Fail'
export const UPDATE_BOKS_NODE_SUCCESS = '[BoksNodes] Update BoksNode Success'

// Action creators
export class UpdateBoksNode implements Action {
  readonly type = UPDATE_BOKS_NODE
  constructor(public payload: BoksNodeElement) {}
}

export class UpdateBoksNodeFail implements Action {
  readonly type = UPDATE_BOKS_NODE_FAIL
  constructor(public payload: any) {}
}

export class UpdateBoksNodeSuccess implements Action {
  readonly type = UPDATE_BOKS_NODE_SUCCESS
  constructor(public payload: BoksNodeElement) {}
}

//
// Delete BoksNode Actions
//
export const DELETE_BOKS_NODE = '[BoksNodes] Delete BoksNode'
export const DELETE_BOKS_NODE_FAIL = '[BoksNodes] Delete BoksNode Fail'
export const DELETE_BOKS_NODE_SUCCESS = '[BoksNodes] Delete BoksNode Success'

// Action creators
export class DeleteBoksNode implements Action {
  readonly type = DELETE_BOKS_NODE
  constructor(public payload: BoksNodeElement) {}
}

export class DeleteBoksNodeFail implements Action {
  readonly type = DELETE_BOKS_NODE_FAIL
  constructor(public payload: any) {}
}

export class DeleteBoksNodeSuccess implements Action {
  readonly type = DELETE_BOKS_NODE_SUCCESS
  constructor(public payload: BoksNodeElement) {}
}

//
// Create BoksContent Actions
//
export const CREATE_BOKS_CONTENT = '[BoksNodes] Create BoksContent'
export const CREATE_BOKS_CONTENT_FAIL = '[BoksNodes] Create BoksContent Fail'
export const CREATE_BOKS_CONTENT_SUCCESS = '[BoksNodes] Create BoksContent Success'

// Action creators
export class CreateBoksContent implements Action {
  readonly type = CREATE_BOKS_CONTENT
  constructor(public payload: BoksContentElement) {}
}

export class CreateBoksContentFail implements Action {
  readonly type = CREATE_BOKS_CONTENT_FAIL
  constructor(public payload: any) {}
}

export class CreateBoksContentSuccess implements Action {
  readonly type = CREATE_BOKS_CONTENT_SUCCESS
  constructor(public payload: BoksContentElement) {}
}

//
// Update BoksContent Actions
//
export const UPDATE_BOKS_CONTENT = '[BoksNodes] Update BoksContent'
export const UPDATE_BOKS_CONTENT_FAIL = '[BoksNodes] Update BoksContent Fail'
export const UPDATE_BOKS_CONTENT_SUCCESS = '[BoksNodes] Update BoksContent Success'

// Action creators
export class UpdateBoksContent implements Action {
  readonly type = UPDATE_BOKS_CONTENT
  constructor(public payload: BoksContentElement) {}
}

export class UpdateBoksContentFail implements Action {
  readonly type = UPDATE_BOKS_CONTENT_FAIL
  constructor(public payload: any) {}
}

export class UpdateBoksContentSuccess implements Action {
  readonly type = UPDATE_BOKS_CONTENT_SUCCESS
  constructor(public payload: BoksContentElement) {}
}

//
// Delete BoksContent Actions
//
export const DELETE_BOKS_CONTENT = '[BoksNodes] Delete BoksContent'
export const DELETE_BOKS_CONTENT_FAIL = '[BoksNodes] Delete BoksContent Fail'
export const DELETE_BOKS_CONTENT_SUCCESS = '[BoksNodes] Delete BoksContent Success'

// Action creators
export class DeleteBoksContent implements Action {
  readonly type = DELETE_BOKS_CONTENT
  constructor(public payload: BoksContentElement) {}
}

export class DeleteBoksContentFail implements Action {
  readonly type = DELETE_BOKS_CONTENT_FAIL
  constructor(public payload: any) {}
}

export class DeleteBoksContentSuccess implements Action {
  readonly type = DELETE_BOKS_CONTENT_SUCCESS
  constructor(public payload: BoksContentElement) {}
}

// action types
export type BoksNodesAction =
  | ResetBoksNodes
  | LoadBoksNodes
  | LoadBoksNodesFail
  | LoadBoksNodesSuccess
  | CreateBoksNode
  | CreateBoksNodeFail
  | CreateBoksNodeSuccess
  | UpdateBoksNode
  | UpdateBoksNodeFail
  | UpdateBoksNodeSuccess
  | DeleteBoksNode
  | DeleteBoksNodeFail
  | DeleteBoksNodeSuccess
  | CreateBoksSubNode
  | CreateBoksSubNodeFail
  | CreateBoksSubNodeSuccess
  | CreateBoksContent
  | CreateBoksContentFail
  | CreateBoksContentSuccess
  | UpdateBoksContent
  | UpdateBoksContentFail
  | UpdateBoksContentSuccess
  | DeleteBoksContent
  | DeleteBoksContentFail
  | DeleteBoksContentSuccess
