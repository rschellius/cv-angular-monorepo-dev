import { ActionReducerMap, createFeatureSelector } from '@ngrx/store'

import * as fromBokses from './boks-node.reducer'
// import * as fromToppings from './toppings.reducer'

// Hier ontstaat de state tree
export interface BoksElementState {
  boksNodeElements: fromBokses.BoksState
  // toppings: fromToppings.ToppingState
}

// De ActionReducerMap zorgt voor typechecking. We kunnen niet zo maar functies toevoegen;
// deze moeten nu uit de BoksElementstate komen.
export const reducers: ActionReducerMap<BoksElementState> = {
  // koppel items aan de reducer function
  boksNodeElements: fromBokses.reducer
  // toppings: fromToppings.reducer,
}

export const getBoksElementsState = createFeatureSelector<BoksElementState>('boks')
