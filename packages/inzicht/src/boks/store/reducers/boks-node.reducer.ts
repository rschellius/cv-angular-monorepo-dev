import * as fromBoks from '../actions'
import { BoksNodeElement, BoksContentElement } from '../../models'

//
// BoksState interface
//
export interface BoksState {
  entities: {
    [id: number]: BoksNodeElement
  }
  loaded: boolean
  loading: boolean
}

//
// Initialisation
//
export const initialState: BoksState = {
  entities: {},
  loaded: false,
  loading: false
}

//
// Reducer function
//
export function reducer(state: BoksState = initialState, action: fromBoks.BoksNodesAction): BoksState {
  switch (action.type) {
    case fromBoks.RESET_BOKS_NODES: {
      return initialState
    }

    case fromBoks.LOAD_BOKS_NODES: {
      return {
        ...state,
        loading: true
      }
    }

    case fromBoks.LOAD_BOKS_NODES_SUCCESS: {
      const boksElements = action.payload
      const entities = boksElements.reduce(
        (_entities: { [id: number]: BoksNodeElement }, boksElement: BoksNodeElement) => {
          return {
            ..._entities,
            [boksElement.id]: boksElement
          }
        },
        {
          ...state.entities
        }
      )
      return {
        ...state,
        loading: false,
        loaded: true,
        entities
      }
    }

    case fromBoks.LOAD_BOKS_NODES_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: false
      }
    }

    case fromBoks.CREATE_BOKS_NODE_SUCCESS:
    case fromBoks.UPDATE_BOKS_NODE_SUCCESS: {
      const boksElement = new BoksNodeElement(action.payload)
      const entities = {
        ...state.entities,
        [boksElement.id]: boksElement
      }
      return {
        ...state,
        entities
      }
    }

    case fromBoks.CREATE_BOKS_SUBNODE_SUCCESS: {
      const boksSubNode = new BoksNodeElement(action.payload)
      const parentId = +boksSubNode.parentId
      const parent = state.entities[parentId]
      const updatedParent = {
        ...parent,
        childNodes: [...parent.childNodes, boksSubNode]
      }

      const entities = {
        ...state.entities,
        [parentId]: updatedParent,
        [boksSubNode.id]: boksSubNode
      }
      return {
        ...state,
        entities
      }
    }

    case fromBoks.DELETE_BOKS_NODE_SUCCESS: {
      const boksElement = action.payload
      // destructure the boksElement from the state object
      // select the boksElement by id and name that 'removed'
      // the remainder are the enteties, without the removed.
      // ES6 destructuring syntax!
      const { [boksElement.id]: removed, ...entities } = state.entities

      return {
        ...state,
        entities
      }
    }

    case fromBoks.CREATE_BOKS_CONTENT_SUCCESS:
    case fromBoks.UPDATE_BOKS_CONTENT_SUCCESS: {
      const boksContent = new BoksContentElement(action.payload)
      const parentId = +boksContent.parentId
      const parent = state.entities[parentId]
      const updatedParent = {
        ...parent,
        contentNodes: [...parent.contentNodes, boksContent]
      }

      console.log('updatedParent = ', updatedParent)
      console.log('boksContent = ', boksContent)

      const entities = {
        ...state.entities,
        [parentId]: updatedParent
      }
      return {
        ...state,
        entities
      }
    }

    case fromBoks.DELETE_BOKS_CONTENT_SUCCESS: {
      const boksContent = action.payload
      // destructure the boksContent from the state object
      // select the boksContent by id and name that 'removed'
      // the remainder are the enteties, without the removed.
      // ES6 destructuring syntax!
      const { [boksContent.id]: removed, ...entities } = state.entities

      return {
        ...state,
        entities
      }
    }

    default: {
      // console.log('Default action! No state change.')
      return state
    }
  }
}

// Selector functions: get the pieces of our state that we need
export const getBoksElementsEntities = (state: BoksState) => state.entities
export const getBoksElementsLoading = (state: BoksState) => state.loading
export const getBoksElementsLoaded = (state: BoksState) => state.loaded
