import { Injectable } from '@angular/core'
import { Actions, ofType, Effect } from '@ngrx/effects'
import { switchMap, map, catchError, tap, take } from 'rxjs/operators'
import { HttpParams } from '@angular/common/http'
import { Store } from '@ngrx/store'
import { of } from 'rxjs'

import { AlertInfo } from '@inzicht/core'
import * as fromRouter from '@inzicht/core/router'
import * as fromCore from '@inzicht/core/src/store'
import { getYear, ContextState } from '@inzicht/core/context'

import * as boksActions from '../actions'
import * as fromServices from '../../services'
import * as fromConstants from '../../boks.constants'
import { BoksContentElement } from '../../models'

@Injectable()
export class BoksNodeEffects {
  constructor(
    private actions$: Actions,
    private readonly store: Store<ContextState>,
    private boksService: fromServices.BoksNodeService
  ) {}

  @Effect()
  loadBoksNodes$ = this.actions$.pipe(
    ofType(boksActions.LOAD_BOKS_NODES),
    switchMap(() => this.store.select(getYear)),
    switchMap(year => {
      const params = new HttpParams().set('year', '' + year)
      return this.boksService.list(params).pipe(
        tap(console.log),
        map(boksNodes => new boksActions.LoadBoksNodesSuccess(boksNodes)),
        catchError(error => of(new boksActions.LoadBoksNodesFail(error)))
      )
    })
  )

  @Effect()
  createBoksNode$ = this.actions$.pipe(
    ofType(boksActions.CREATE_BOKS_NODE),
    map((action: boksActions.CreateBoksNode) => action.payload),
    switchMap(fromPayload =>
      this.boksService.create(fromPayload).pipe(
        map(boksElement => new boksActions.CreateBoksNodeSuccess(boksElement)),
        catchError(error => of(new boksActions.CreateBoksNodeFail(error)))
      )
    )
  )

  @Effect()
  createBoksNodeSuccess$ = this.actions$.pipe(
    ofType(boksActions.CREATE_BOKS_NODE_SUCCESS),
    map((action: boksActions.CreateBoksNodeSuccess) => action.payload),
    switchMap(boksElement => [
      new fromCore.AlertSuccess(new AlertInfo('Success', `${boksElement.name} aangemaakt`)),
      new fromRouter.Go({
        path: [fromConstants.ROUTE_NODES, boksElement.id]
      })
    ])
  )

  @Effect()
  createBoksSubNode$ = this.actions$.pipe(
    ofType(boksActions.CREATE_BOKS_SUBNODE),
    map((action: boksActions.CreateBoksSubNode) => action.payload),
    switchMap(fromPayload =>
      this.boksService.createSubNode(fromPayload).pipe(
        map(boksElement => new boksActions.CreateBoksSubNodeSuccess(boksElement)),
        catchError(error => of(new boksActions.CreateBoksSubNodeFail(error)))
      )
    )
  )

  @Effect()
  createBoksSubNodeSuccess$ = this.actions$.pipe(
    ofType(boksActions.CREATE_BOKS_SUBNODE_SUCCESS),
    map((action: boksActions.CreateBoksSubNodeSuccess) => action.payload),
    switchMap(boksSubNode => [
      new fromCore.AlertSuccess(new AlertInfo('Success', `${boksSubNode.name} aangemaakt`)),
      new fromRouter.Go({
        path: [fromConstants.ROUTE_NODES, boksSubNode.parentId]
      })
    ])
  )

  @Effect()
  createBoksContent$ = this.actions$.pipe(
    ofType(boksActions.CREATE_BOKS_CONTENT),
    map((action: boksActions.CreateBoksContent) => action.payload),
    switchMap(fromPayload =>
      this.boksService.createContentNode(fromPayload).pipe(
        map((boksContent: BoksContentElement) => new boksActions.CreateBoksContentSuccess(boksContent)),
        catchError(error => of(new boksActions.CreateBoksContentFail(error)))
      )
    )
  )

  @Effect()
  createBoksContentSuccess$ = this.actions$.pipe(
    ofType(boksActions.CREATE_BOKS_CONTENT_SUCCESS),
    map((action: boksActions.CreateBoksContentSuccess) => action.payload),
    switchMap(boksContent => [
      new fromCore.AlertSuccess(new AlertInfo('Success', `${boksContent.name} aangemaakt`))
      // new fromRouter.Go({
      //   path: [fromConstants.ROUTE_NODES, boksContent.parentId]
      // })
    ])
  )

  @Effect()
  updateBoksContent$ = this.actions$.pipe(
    ofType(boksActions.UPDATE_BOKS_CONTENT),
    map((action: boksActions.UpdateBoksContent) => action.payload),
    switchMap(fromPayload =>
      this.boksService.updateContentNode(fromPayload).pipe(
        map((boksContent: BoksContentElement) => new boksActions.CreateBoksContentSuccess(boksContent)),
        catchError(error => of(new boksActions.CreateBoksContentFail(error)))
      )
    )
  )

  @Effect()
  updateBoksContentSuccess$ = this.actions$.pipe(
    ofType(boksActions.UPDATE_BOKS_CONTENT_SUCCESS),
    map((action: boksActions.CreateBoksContentSuccess) => action.payload),
    switchMap(boksContent => [
      new fromCore.AlertSuccess(new AlertInfo('Success', `${boksContent.name} aangemaakt`))
      // new fromRouter.Go({
      //   path: [fromConstants.ROUTE_NODES, boksContent.parentId]
      // })
    ])
  )

  @Effect()
  updateBoksNode$ = this.actions$.pipe(
    ofType(boksActions.UPDATE_BOKS_NODE),
    map((action: boksActions.UpdateBoksNode) => action.payload),
    switchMap(fromPayload =>
      this.boksService.update(fromPayload).pipe(
        map(boksElement => new boksActions.UpdateBoksNodeSuccess(boksElement)),
        catchError(error => of(new boksActions.UpdateBoksNodeFail(error)))
      )
    )
  )

  @Effect()
  deleteBoksNode$ = this.actions$.pipe(
    ofType(boksActions.DELETE_BOKS_NODE),
    map((action: boksActions.DeleteBoksNode) => action.payload),
    switchMap(boksElement =>
      this.boksService.delete(boksElement.id).pipe(
        // boksService.remove returns nothing, so we return
        // the deleted boksElement ourselves on success
        map(() => new boksActions.DeleteBoksNodeSuccess(boksElement)),
        catchError(error => of(new boksActions.DeleteBoksNodeFail(error)))
      )
    )
  )

  @Effect()
  loadBoksNodesFail$ = this.actions$.pipe(
    ofType(
      boksActions.LOAD_BOKS_NODES_FAIL,
      boksActions.CREATE_BOKS_NODE_FAIL,
      boksActions.UPDATE_BOKS_NODE_FAIL,
      boksActions.DELETE_BOKS_NODE_FAIL,
      boksActions.CREATE_BOKS_SUBNODE_FAIL,
      boksActions.CREATE_BOKS_CONTENT_FAIL,
      boksActions.UPDATE_BOKS_CONTENT_FAIL,
      boksActions.DELETE_BOKS_CONTENT_FAIL
    ),
    map((action: any) => action.payload),
    map(info => new fromCore.AlertError(new AlertInfo(info.title, info.message)))
  )

  @Effect()
  handleBoksNodeSuccess$ = this.actions$.pipe(
    ofType(boksActions.UPDATE_BOKS_NODE_SUCCESS, boksActions.DELETE_BOKS_NODE_SUCCESS),
    switchMap(() => [
      new fromCore.AlertSuccess(new AlertInfo('Success', 'Item was updated.')),
      new fromRouter.Go({
        path: [fromConstants.ROUTE_NODES]
      })
    ])
  )
}
