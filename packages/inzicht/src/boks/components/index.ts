import { BoksFormComponent } from './boks-form/boks-form.component'
import { LearningLineSelectorComponent } from './learningline-selector/learningline-selector.component'
import { topnavComponents } from './boks-topnav'

export const components: any[] = [topnavComponents, BoksFormComponent, LearningLineSelectorComponent]

export * from './boks-topnav'
export * from './boks-form/boks-form.component'
export * from './learningline-selector/learningline-selector.component'
