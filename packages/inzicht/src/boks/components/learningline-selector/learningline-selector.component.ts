import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'
import { Observable } from 'rxjs'
import { Store } from '@ngrx/store'
import { BoksNodeElement } from '../../models'

import * as fromStore from '../../store'
import { filter, map } from 'rxjs/operators'
import { FormBuilder, FormGroup, FormControl } from '@angular/forms'
import { Objective } from '../../../objective/models'

@Component({
  selector: 'cv-learningline-selector',
  template: `
    <h2>Leerlijnen</h2>
    <p>Selecteer hier één of meer leerlijnen.</p>

    <div class="form-group col-xs">
      <form #f="ngForm">
        <div *ngFor="let category of categories">
          <input
            type="checkbox"
            [ngModelOptions]="{ standalone: true }"
            (change)="onSelectionChanged($event)"
            [(ngModel)]="category.selected"
          />
          {{ category.title }}
        </div>
      </form>
    </div>
  `
})
export class LearningLineSelectorComponent implements OnInit {
  @Input() objective: Objective
  @Output() selectionChanged = new EventEmitter<any[]>()
  //
  learninglines$: Observable<BoksNodeElement[]>
  categories = [
    { title: 'Food', selected: false },
    { title: 'Cars', selected: true },
    { title: 'Shopping', selected: false }
  ]

  constructor(private readonly store: Store<fromStore.BoksElementState>, private readonly fb: FormBuilder) {}

  ngOnInit() {
    this.learninglines$ = this.store
      .select(fromStore.getAllBoksElements)
      .pipe(map(entities => entities.filter(item => item.isLearningLine)))
  }

  // getSelectedCategories() {
  //   const selected = this.categories.filter(c => c.selected)
  //   console.log(selected)
  // }

  onSelectionChanged(event: Event) {
    console.log('selection changed', event)
    // this.selectionChanged.emit(event)
  }
}
