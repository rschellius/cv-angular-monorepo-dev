import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { environment } from '@inzicht/inzicht'

import { EntityService } from '@inzicht/core'
import { BoksContentElement } from '../models/boks.model'

@Injectable()
export class BoksContentService extends EntityService<BoksContentElement> {
  constructor(httpClient: HttpClient) {
    super(httpClient, environment.API_BASE_URL, 'bokscontent')
  }
}
