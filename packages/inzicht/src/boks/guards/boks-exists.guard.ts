import { Injectable } from '@angular/core'
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router'
import { Store } from '@ngrx/store'
import { Observable } from 'rxjs'
import { tap, filter, take, switchMap, map, flatMap } from 'rxjs/operators'

import * as fromStore from '../store'
import * as fromRouter from '@inzicht/core/router'
import { BoksNodeElement } from '../models'

@Injectable()
export class BoksNodeExistsGuard implements CanActivate {
  constructor(private store: Store<fromStore.BoksElementState>) {}

  /**
   * Check wether the item that we navigated to exists.
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.checkStore().pipe(
      switchMap(() => this.store.select(fromRouter.getRouterState)),
      flatMap(router => router.state.params.filter((param: { [x: string]: any }) => param['boksId'])),
      switchMap((param: any) => this.hasBoksElement(+param.boksId))
    )
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(route, state)
  }

  hasBoksElement(id: number): Observable<boolean> {
    return this.store.select(fromStore.getBoksElementsEntities).pipe(
      // Get the entity with given id, and convert to boolean
      // true or false indicates wether the item exists.
      map((entities: { [key: number]: BoksNodeElement }) => !!entities[id]),
      tap(result => console.log(`hasBoksElement ${id} = ${result}`)),
      take(1)
    )
  }

  checkStore(): Observable<boolean> {
    return this.store.select(fromStore.getBoksElementsLoaded).pipe(
      tap(loaded => {
        if (!loaded) {
          // Not loaded, so reload from the store
          this.store.dispatch(new fromStore.LoadBoksNodes())
        }
      }),
      // this filter construct waits for loaded to become true
      filter(loaded => loaded),
      // this take completes the observable and unsubscribes
      take(1)
    )
  }
}
