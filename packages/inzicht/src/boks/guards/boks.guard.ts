import { Injectable } from '@angular/core'
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router'

import { Store } from '@ngrx/store'
import { Observable, of } from 'rxjs'
import { map, tap, filter, take, switchMap, catchError } from 'rxjs/operators'

import * as fromStore from '../store'

@Injectable()
export class BoksGuard implements CanActivate {
  constructor(private store: Store<fromStore.BoksElementState>) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.checkStore().pipe(
      switchMap(() => of(true)),
      catchError(() => of(false))
    )
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(route, state)
  }

  checkStore(): Observable<boolean> {
    return this.store.select(fromStore.getBoksElementsLoaded).pipe(
      tap(loaded => {
        if (!loaded) {
          // load items from the store
          this.store.dispatch(new fromStore.LoadBoksNodes())
        }
      }),
      // this filter construct waits for loaded to become true
      filter(loaded => loaded),
      // this take completes the observable and unsubscribes
      take(1)
    )
  }
}
