import { BoksGuard } from './boks.guard'
import { BoksNodeExistsGuard } from './boks-exists.guard'

export const guards: any[] = [BoksGuard, BoksNodeExistsGuard]

export * from './boks.guard'
export * from './boks-exists.guard'
