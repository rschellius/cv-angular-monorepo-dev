import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { Observable, Subscription } from 'rxjs'
import { Store } from '@ngrx/store'

import { getIsAuthenticated } from '@inzicht/core/src/store'
import { BaseComponent } from '@inzicht/core'
import { BoksNodeElement } from '../../models'

import * as fromStore from '../../store'

@Component({
  selector: 'cv-boks-content-edit',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './boks-content.edit.component.html'
})
export class BoksContentEditComponent extends BaseComponent implements OnInit {
  // Page title
  title: string

  // The core element of this Component
  boksNodeElements$: Observable<BoksNodeElement[]>

  // User authentication role
  userMayEdit$: Observable<boolean>

  constructor(private readonly store: Store<fromStore.BoksElementState>) {
    super()
  }

  ngOnInit() {
    this.userMayEdit$ = this.store.select(getIsAuthenticated)
    this.boksNodeElements$ = this.store.select(fromStore.getBoksElementsAsTree)
  }

  onItemDropped(event) {
    console.log('onItemDropped', event)
  }
}
