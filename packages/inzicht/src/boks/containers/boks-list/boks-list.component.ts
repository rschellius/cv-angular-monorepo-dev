import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { Observable } from 'rxjs'
import { NGXLogger } from 'ngx-logger'
import { Store } from '@ngrx/store'

import { ActionsGroup } from '@inzicht/ui'
import { getIsAuthenticated } from '@inzicht/core'

import * as fromStore from '../../store'
import * as fromConstants from '../../boks.constants'

import { BoksNodeElement } from '../../models'
import { BoksComponent } from '../../containers'

@Component({
  selector: 'cv-boks-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './boks-list.component.html'
})
export class BoksListComponent implements OnInit {
  // Class name for logging
  static TAG = BoksListComponent.name

  title = 'BOKS Nodes'

  // The core element of this Component
  boksNodeElements$: Observable<BoksNodeElement[]>

  // User authentication role
  userMayEdit$: Observable<boolean>

  columnHeaders = [
    // { prop: 'nr', name: 'Nr.', flexGrow: 1 },
    { prop: 'id', name: 'Id', flexGrow: 1 },
    { prop: 'name', name: 'Naam', flexGrow: 3 },
    { prop: 'isLearningLine', name: 'Is Leerlijn', flexGrow: 1 },
    { prop: 'parent.name', name: 'Onderdeel van', flexGrow: 3 },
    { prop: 'childNodes.length', name: '# Nodes', flexGrow: 1 }
  ]

  selected: any = []

  /**
   * Actions op deze page
   */
  actions: ActionsGroup[] = [
    new ActionsGroup({
      title: 'Acties',
      actions: [
        {
          name: 'Element toevoegen',
          routerLink: 'new'
        },
        {
          name: 'BOKS overzicht',
          routerLink: 'todo'
        }
      ]
    }),
    new ActionsGroup({
      title: 'Andere acties',
      actions: [
        {
          name: 'Elementen verwijderen',
          routerLink: 'edit'
        }
      ]
    })
  ]

  constructor(
    private readonly store: Store<fromStore.BoksElementState>,
    private readonly logger: NGXLogger,
    private readonly router: Router,
    private readonly route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.userMayEdit$ = this.store.select(getIsAuthenticated)
    this.boksNodeElements$ = this.store.select(fromStore.getAllBoksElements)
  }

  onAddElement() {
    this.logger.debug(BoksListComponent.TAG, 'Add element')
    this.router.navigate([fromConstants.ROUTE_NODES, 'new'])
  }

  onSelect({ selected }) {
    if (selected.length === 1) {
      const element = selected[0] as BoksNodeElement
      this.logger.debug(BoksListComponent.TAG, 'onSelect id = ' + element.id)
      this.router.navigate([element.id], { relativeTo: this.route })
    }
  }
}
