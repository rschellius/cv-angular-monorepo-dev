import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { Observable, Subscription } from 'rxjs'
import { Store } from '@ngrx/store'

import { ActionsGroup } from '@inzicht/ui'
import { getIsAuthenticated } from '@inzicht/core/src/store/selectors'

import { BoksNodeElement } from '../../models'
import * as fromStore from '../../store'
import * as fromConstants from '../../boks.constants'

@Component({
  selector: 'cv-boks-detail',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './boks-detail.component.html'
})
export class BoksDetailComponent implements OnInit {
  // Logging classname tag
  readonly TAG = BoksDetailComponent.name

  // Page title
  title: string

  // The core element of this Component
  boksElement$: Observable<BoksNodeElement>

  // User authentication role
  userMayEdit$: Observable<boolean>

  // Subscription on observable
  subscription: Subscription

  /**
   * Actions on this page
   */
  actions: ActionsGroup[] = [
    new ActionsGroup({
      title: 'Acties',
      actions: [
        {
          name: 'Toevoegen',
          routerLink: 'new'
        },
        {
          name: 'Verwijderen',
          routerLink: 'todo'
        }
      ]
    })
  ]

  constructor(
    private readonly store: Store<fromStore.BoksElementState>,
    private readonly router: Router,
    private readonly route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.userMayEdit$ = this.store.select(getIsAuthenticated)
    this.boksElement$ = this.store.select(fromStore.getSelectedBoksElement)
  }

  onEditElement(id: number) {
    this.router.navigate([fromConstants.ROUTE_NODES, id, 'edit'])
  }

  onAddSubNodeElement(id: number) {
    this.router.navigate(['addnode'], { relativeTo: this.route })
  }

  onAddContentElement(id: number) {
    this.router.navigate(['addcontent'], { relativeTo: this.route })
  }

  onSelectSubElement(id: number) {
    this.router.navigate([fromConstants.ROUTE_NODES, id])
  }
}
