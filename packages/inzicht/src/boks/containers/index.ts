import { BoksComponent } from './boks.component'
import { BoksDetailComponent } from './boks-detail/boks-detail.component'
import { BoksListComponent } from './boks-list/boks-list.component'
import { BoksNodeEditComponent } from './boks-edit/boks-node-edit.component'
import { BoksSubnodeCreateComponent } from './boks-edit/boks-subnode-create.component'
import { BoksContentCreateComponent } from './boks-edit/boks-content-create.component'
import { BoksContentEditComponent } from './boks-content-edit/boks-content.edit.component'

export const containers: any[] = [
  BoksComponent,
  BoksDetailComponent,
  BoksContentEditComponent,
  BoksListComponent,
  BoksNodeEditComponent,
  BoksSubnodeCreateComponent,
  BoksContentCreateComponent
]

export * from './boks.component'
export * from './boks-content-edit/boks-content.edit.component'
export * from './boks-detail/boks-detail.component'
export * from './boks-edit/boks-node-edit.component'
export * from './boks-list/boks-list.component'
export * from './boks-edit/boks-subnode-create.component'
export * from './boks-edit/boks-content-create.component'
