import { Component } from '@angular/core'

@Component({
  selector: 'cv-boks',
  template: `
    <cv-boks-topnav></cv-boks-topnav>
    <div class="app">
      <div class="app__content">
        <div class="app__container">
          <router-outlet></router-outlet>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['boks.component.scss']
})
export class BoksComponent {}
