import { createSelector } from '@ngrx/store'

import * as fromRouter from '@inzicht/core/router'
import * as fromFeature from '../reducers'
import * as fromModules from '../reducers/module.reducer'
import { Module } from '../../models'

//
// Selectors - zijn nodig om door delen van de state tree te navigeren
// They return slices of the state tree.
//
export const getModuleState = createSelector(
  fromFeature.getModulesState,
  (state: fromFeature.ModuleState) => state.moduleElements
)

export const getModulesEntities = createSelector(
  getModuleState,
  fromModules.getModulesEntities
)

// Get the selected item based on id from the route
export const getSelectedModule = createSelector(
  getModulesEntities,
  fromRouter.getRouterState,
  (entities, router): Module => {
    const params: any[] = router.state.params.filter(item => item['moduleId'])
    return router.state && params && params[0] && entities[params[0]['moduleId']]
  }
)

export const getAllModules = createSelector(
  getModulesEntities,
  entities => {
    // Return an array version of our entities object
    // so that we can iterate over it via ngFor in HTML.
    return Object.keys(entities).map(id => entities[parseInt(id, 10)])
  }
)

export const getModulesLoading = createSelector(
  getModuleState,
  fromModules.getModulesLoading
)

export const getModulesLoaded = createSelector(
  getModuleState,
  fromModules.getModulesLoaded
)
