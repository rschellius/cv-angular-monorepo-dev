import * as fromModule from '../actions'
import { Module } from '../../models'

//
// ModuleState interface
//
export interface ModuleState {
  entities: {
    [id: number]: Module
  }
  loaded: boolean
  loading: boolean
}

//
// Initialisation
//
export const initialState: ModuleState = {
  entities: {},
  loaded: false,
  loading: false
}

//
//
//
export function reducer(state: ModuleState = initialState, action: fromModule.ModulesAction): ModuleState {
  switch (action.type) {
    case fromModule.RESET_MODULES: {
      return initialState
    }

    case fromModule.LOAD_MODULES: {
      return {
        ...state,
        loading: true
      }
    }

    case fromModule.LOAD_MODULES_SUCCESS: {
      const moduleElements = action.payload
      const entities = moduleElements.reduce(
        (
          entities: { [id: number]: Module },
          moduleElement: Module
          // entities, moduleElement
        ) => {
          return {
            ...entities,
            [moduleElement.id]: moduleElement
          }
        },
        {
          ...state.entities
        }
      )
      return {
        ...state,
        loading: false,
        loaded: true,
        entities
      }
    }

    case fromModule.LOAD_MODULES_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: false
      }
    }

    case fromModule.CREATE_MODULE_SUCCESS:
    case fromModule.UPDATE_MODULE_SUCCESS: {
      const moduleElement = action.payload
      const entities = {
        ...state.entities,
        [moduleElement.id]: moduleElement
      }
      return {
        ...state,
        entities
      }
    }

    case fromModule.DELETE_MODULE_SUCCESS: {
      const moduleElement = action.payload
      // destructure the moduleElement from the state object
      // select the moduleElement by id and name that 'removed'
      // the remainder are the enteties, without the removed.
      // ES6 destructuring syntax!
      const { [moduleElement.id]: removed, ...entities } = state.entities

      return {
        ...state,
        entities
      }
    }

    default: {
      return state
    }
  }
}

// Selector functions: get the pieces of our state that we need
export const getModulesEntities = (state: ModuleState) => state.entities
export const getModulesLoading = (state: ModuleState) => state.loading
export const getModulesLoaded = (state: ModuleState) => state.loaded
