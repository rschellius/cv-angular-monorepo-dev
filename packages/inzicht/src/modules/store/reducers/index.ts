import { ActionReducerMap, createFeatureSelector } from '@ngrx/store'
import * as fromModulees from './module.reducer'

// Hier ontstaat de state tree
export interface ModuleState {
  moduleElements: fromModulees.ModuleState
}

// De ActionReducerMap zorgt voor typechecking. We kunnen niet zo maar functies toevoegen;
// deze moeten nu uit de Modulestate komen.
export const reducers: ActionReducerMap<ModuleState> = {
  // koppel items aan de reducer function
  moduleElements: fromModulees.reducer
}

export const getModulesState = createFeatureSelector<ModuleState>('module')
