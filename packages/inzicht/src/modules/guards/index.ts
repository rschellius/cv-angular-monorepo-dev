import { ModuleGuard } from './module.guard'
import { ModuleExistsGuard } from './module-exists.guard'

export const guards: any[] = [ModuleGuard, ModuleExistsGuard]

export * from './module.guard'
export * from './module-exists.guard'
