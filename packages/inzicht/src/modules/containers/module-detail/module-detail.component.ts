import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { Observable, Subscription } from 'rxjs'
import { Store } from '@ngrx/store'
import { NGXLogger } from 'ngx-logger'

import { BaseComponent } from '@inzicht/core'
import { ActionsGroup } from '@inzicht/ui'
import * as fromAuth from '@inzicht/core'

import { Module } from '../../models'
import * as fromStore from '../../store'
import * as fromConstants from '../../module.constants'

@Component({
  selector: 'cv-module-detail',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './module-detail.component.html'
})
export class ModuleDetailComponent extends BaseComponent implements OnInit, OnDestroy {
  // Page title
  title: string

  subtitle: string

  // The core element of this Component
  module: Module

  // User authentication role
  userMayEdit$: Observable<boolean>

  // Subscription on observable
  subscription: Subscription

  /**
   * Actions on this page
   */
  actions: ActionsGroup[] = [
    new ActionsGroup({
      title: 'Acties',
      actions: [
        {
          name: 'Toevoegen',
          routerLink: 'new'
        },
        {
          name: 'Verwijderen',
          routerLink: 'todo'
        }
      ]
    })
  ]

  constructor(
    private readonly store: Store<fromStore.ModuleState>,
    private readonly router: Router,
    private readonly route: ActivatedRoute
  ) {
    super()
  }

  ngOnInit() {
    this.userMayEdit$ = this.store.select(fromAuth.getIsAuthenticated)

    this.subscription = this.store.select(fromStore.getSelectedModule).subscribe((result: Module) => {
      this.module = result
      this.subtitle =
        'Onderdeel van ' +
        this.module.course.name +
        ' (jaar ' +
        this.module.course.studyYear +
        ', periode ' +
        this.module.course.trimester +
        ')'
    })
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe()
  }

  onEdit(id: number) {
    this.router.navigate([fromConstants.BASE_ROUTE, id, 'edit'])
  }
}
