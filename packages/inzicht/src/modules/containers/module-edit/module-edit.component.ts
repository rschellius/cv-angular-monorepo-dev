import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { ActionsGroup } from '@inzicht/ui'
import { Store } from '@ngrx/store'
import { Observable, of } from 'rxjs'
import { tap } from 'rxjs/operators'

import * as fromModels from '../../models'
import * as fromStore from '../../store'
import { DialogService } from '@inzicht/users'

@Component({
  selector: 'cv-module-edit',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './module-edit.component.html',
  styleUrls: []
})
export class ModuleEditComponent implements OnInit {
  title: string

  // Item to be edited
  module$: Observable<fromModels.Module>

  /**
   * Actions op deze page
   */
  actions: ActionsGroup[] = [
    new ActionsGroup({
      title: 'Acties',
      actions: [
        {
          name: 'Annuleren',
          routerLink: '..'
        }
      ]
    })
  ]

  constructor(
    private readonly store: Store<fromStore.ModuleState>,
    private readonly dialogService: DialogService,
    private readonly route: ActivatedRoute,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.title = this.route.snapshot.data['title'] || 'Nieuwe onderwijseenheid'

    this.module$ = this.store.select(fromStore.getSelectedModule).pipe(
      tap((module: fromModels.Module = null) => {
        const moduleExists = !!module
      })
    )
  }

  onCreate(event: fromModels.Module) {
    this.store.dispatch(new fromStore.CreateModule(event))
  }

  onUpdate(event: fromModels.Module) {
    this.store.dispatch(new fromStore.UpdateModule(event))
  }

  onRemove(event: fromModels.Module) {
    const remove = window.confirm('Are you sure?')
    if (remove) {
      this.store.dispatch(new fromStore.DeleteModule(event))
    }
  }

  onCancel() {
    this.router.navigate(['..'], { relativeTo: this.route })
  }

  canDeactivate() {
    // Allow synchronous navigation (`true`) if no crisis or the crisis is unchanged
    //   if (!this.form.dirty || this.form.untouched) {
    //     console.log('not dirty or untouched')
    //     return true
    //   }
    return this.dialogService.confirm(
      'Je hebt mogelijk wijzigingen in het formulier aangebracht. Wijzigingen weggooien?'
    )
    // }
  }
}
