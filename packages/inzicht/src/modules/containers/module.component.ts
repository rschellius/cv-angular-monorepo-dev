import { Component } from '@angular/core'

@Component({
  selector: 'cv-module',
  template: `
    <cv-module-topnav></cv-module-topnav>
    <div class="app">
      <div class="app__content">
        <div class="app__container">
          <router-outlet></router-outlet>
        </div>
      </div>
    </div>
  `
})
export class ModuleComponent {}
