import { Component, Input } from '@angular/core'
import { Module } from '../../models'

@Component({
  selector: 'cv-module-info',
  template: `
    <h3>{{ title }}</h3>
    <table class="table">
      <tbody>
        <tr>
          <td>Naam</td>
          <td>
            <a [routerLink]="['/periodes', module.course.id]">
              {{ module.course.studyYear }}.{{ module.course.trimester }} {{ module.course.name }}
            </a>
          </td>
        </tr>
        <tr>
          <td>Jaar</td>
          <td>{{ module.course.studyYear }}</td>
        </tr>
        <tr>
          <td>Periode</td>
          <td>{{ module.course.trimester }}</td>
        </tr>
        <tr>
          <td>ECs</td>
          <td>{{ module.credits }}</td>
        </tr>
        <tr>
          <td>Vorm</td>
          <td>{{ module.form }}</td>
        </tr>
        <tr>
          <td>Contact</td>
          <td>
            {{ module.responsiblePerson.firstName }}
            {{ module.responsiblePerson.lastName }}
          </td>
        </tr>
      </tbody>
    </table>
  `,
  styles: ['.table td { padding: 8px; font-size: 0.9em; }']
})
export class ModuleInfoComponent {
  title = 'Onderwijseenheid info'
  @Input() module: Module
}
