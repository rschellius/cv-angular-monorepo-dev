import { ModuleFormComponent } from './module-form/module-form.component'
import { ModuleInfoComponent } from './module-info/module-info.component'
import { topnavComponents } from './module-topnav'

export const components: any[] = [topnavComponents, ModuleFormComponent, ModuleInfoComponent]

export * from './module-topnav'
export * from './module-form/module-form.component'
export * from './module-info/module-info.component'
