import { NgModule } from '@angular/core'
import { HttpModule } from '@angular/http'
import { ModuleWithProviders } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { QuillModule } from 'ngx-quill'
import { NgxDatatableModule } from '@swimlane/ngx-datatable'
import { StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'

import * as fromAuth from '@inzicht/core'
import { CoreModule } from '@inzicht/core'
import { PersonModule } from '@inzicht/users'
import { UIModule } from '@inzicht/ui'

import { ModuleRoutingModule } from './module.routing'
import { reducers, effects } from './store'
import * as fromComponents from './components'
import * as fromContainers from './containers'
import * as fromGuards from './guards'
import * as fromServices from './services'
import { ObjectiveModule } from '../objective/objective.module'
import { CourseModule } from '../course/course.module'

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    CoreModule,
    UIModule,
    CourseModule,
    ObjectiveModule,
    PersonModule,
    QuillModule,
    NgbModule,
    // Lazyload all store parts
    StoreModule.forFeature('module', reducers),
    EffectsModule.forFeature(effects),
    ModuleRoutingModule
  ],
  declarations: [...fromContainers.containers, ...fromComponents.components],
  providers: [...fromServices.services, ...fromGuards.guards, ...fromAuth.guards],
  exports: [...fromContainers.containers, ...fromComponents.components]
})
export class ModuleModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ModuleModule,
      providers: [...fromServices.services]
    }
  }
}
