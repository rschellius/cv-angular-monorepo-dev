// tslint:disable: max-line-length
import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgModule, LOCALE_ID } from '@angular/core'
import { registerLocaleData, APP_BASE_HREF } from '@angular/common'
import localeNl from '@angular/common/locales/nl'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule, HttpClientXsrfModule } from '@angular/common/http'
import { StoreDevtoolsModule } from '@ngrx/store-devtools'
import { storeFreeze } from 'ngrx-store-freeze'
import { environment } from '@inzicht/inzicht'
import { LoggerModule } from 'ngx-logger'
import { QuillModule } from 'ngx-quill'
import { StoreRouterConnectingModule, RouterStateSerializer } from '@ngrx/router-store'
import { StoreModule, MetaReducer } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'
import { ToastrModule } from 'ngx-toastr'

import { AppRoutingModule } from './app-routing.module'
import { MainComponent } from './app/containers/main/main.component'
import { AppDashboardComponent } from './app/containers/dashboard/dashboard.component'
import { StyleExampleColumnsComponent } from './style-examples/style-example-columns.component'
import { StyleExampleWideComponent } from './style-examples/style-example-wide.component'
import { StyleExampleFullWidthComponent } from './style-examples/style-example-fullwidth.component'
import { StyleExample2Component } from './style-examples/style-example-2.component'

import { reducers, effects, CustomSerializer } from '@inzicht/core/router'
import { ContextModule } from '@inzicht/core/context'
import { CoreModule } from '@inzicht/core'
import { UIModule } from '@inzicht/ui'
import { PersonModule } from '@inzicht/users'

import { EndqualificationModule } from './endqualification/endqualification.module'
import { BoksModule } from './boks/boks.module'
import { ObjectiveModule } from './objective/objective.module'
import { CourseModule } from './course/course.module'
import { ModuleModule } from './modules/module.module'

export const metaReducers: MetaReducer<any>[] = !environment.production ? [storeFreeze] : []

registerLocaleData(localeNl, 'nl')

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: 'toast-top-right',
      preventDuplicates: false,
      maxOpened: 5
    }),
    NgbModule,
    CoreModule,
    UIModule,
    LoggerModule.forRoot(environment.logConfig),
    QuillModule.forRoot(environment.quillConfig),
    // Always import the HttpClientInMemoryWebApiModule after the HttpClientModule
    // to ensure that the in-memory backend provider supersedes the Angular version.
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'My-Xsrf-Cookie',
      headerName: 'My-Xsrf-Header'
    }),
    // Conditional import! Production does not use the in-memory web api.
    // environment.production ? [] :
    // HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, {
    //   dataEncapsulation: false,
    //   passThruUnknownUrl: true,
    //   put204: false // return entity after PUT/update
    // }),
    StoreModule.forRoot(reducers, { metaReducers }),
    EffectsModule.forRoot(effects),
    StoreRouterConnectingModule.forRoot(),
    environment.production ? [] : StoreDevtoolsModule.instrument(),

    ContextModule,
    BoksModule.forRoot(),
    ObjectiveModule.forRoot(),
    EndqualificationModule.forRoot(),
    CourseModule.forRoot(),
    PersonModule.forRoot(),
    ModuleModule.forRoot(),

    // AuthModule.forRoot(),
    AppRoutingModule
  ],
  declarations: [
    MainComponent,
    AppDashboardComponent,
    StyleExampleColumnsComponent,
    StyleExampleWideComponent,
    StyleExampleFullWidthComponent,
    StyleExample2Component
  ],
  providers: [
    { provide: RouterStateSerializer, useClass: CustomSerializer },
    { provide: APP_BASE_HREF, useValue: '/' },
    { provide: LOCALE_ID, useValue: 'nl' }
  ],
  bootstrap: [MainComponent]
})
export class AppModule {}
