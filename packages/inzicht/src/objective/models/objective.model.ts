import { Entity } from '@inzicht/core'

import { Qualification, Endqualification } from '../../endqualification/models'
import { BoksNodeElement } from '../../boks/models'
import { Module } from '../../modules/models'
import { Course } from '../../course/models'

export enum BloomTaxonomy {
  REMEMBER = 'Remember',
  UNDERSTAND = 'Understand',
  APPLY = 'Apply',
  ANALYZE = 'Analyze',
  EVALUATE = 'Evaluate',
  CREATE = 'Create',
  NOT_APPLICABLE = 'Niet van toepassing'
}

export enum HboIArchitectureLayer {
  SOFTWARE = 'Software',
  USER_INTERACTION = 'Gebruikersinteractie',
  INFRASTRUCTURE = 'Infrastructure',
  BUSINESS_PROCESSES = 'Bedrijfsprocessen',
  NOT_APPLICABLE = 'Niet van toepassing'
}

export enum DomainCompetence {
  REALIZING = 'Realiseren',
  DESIGNING = 'Ontwerpen',
  NOT_APPLICABLE = 'Niet van toepassing'
}

/**
 * Leerdoel.
 *
 */
export class Objective extends Entity {
  // Het leerdoel. Name dekt de lading niet helemaal
  name!: string

  // Beschrijving/toelichting
  shortDescription!: string

  // Beschrijving/toelichting
  longDescription: string

  // Het vak dat dit leerdoel implementeert.
  module: Module

  // Leerlijn(en) waarin dit leerdoel gerealiseerd wordt
  learningLines!: BoksNodeElement[]

  // Array van eindkwalificaties waaraan dit leerdoel bijdraagt
  endqualifications: Endqualification[]

  bloomTaxonomy: BloomTaxonomy = BloomTaxonomy.NOT_APPLICABLE

  hboIArchitectureLayer: HboIArchitectureLayer[] = []

  constructor(values: any = {}) {
    super(values)
    try {
      this.name = values['name']
      this.shortDescription = values['shortDescription']
      this.longDescription = values['longDescription']
      this.bloomTaxonomy = values['bloomTaxonomy']
      this.hboIArchitectureLayer = values['hboIArchitectureLayer']
      this.endqualifications = values['endqualifications']
      this.module = values['module']
      this.learningLines = values['learningLine']
    } catch (e) {
      console.error('Error in constructor: ' + e.toString())
    }
  }
}
