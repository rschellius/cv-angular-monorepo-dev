import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import * as fromAuth from '@inzicht/core'

import * as fromEndqualificationsGuards from '../endqualification/guards'
import * as fromGuards from './guards'
import * as fromContainers from './containers'
import * as fromBoksGuards from '../boks/guards'
import * as fromConstants from './objective.constants'

const routes: Routes = [
  {
    path: fromConstants.BASE_ROUTE,
    component: fromContainers.ObjectiveComponent,
    canActivate: [fromGuards.ObjectiveGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: fromContainers.ObjectiveListComponent
      },
      {
        path: 'new',
        component: fromContainers.ObjectiveEditComponent,
        canActivate: [
          fromAuth.IsLoggedInGuard,
          fromEndqualificationsGuards.EndqualificationGuard,
          fromBoksGuards.BoksGuard
        ],
        canDeactivate: [fromAuth.CanDeactivateGuard],
        data: {
          title: 'Nieuw Leerdoel',
          optionalLearningline: true
        }
      },
      {
        path: ':objectiveId',
        component: fromContainers.ObjectiveDetailComponent,
        canActivate: [fromGuards.ObjectiveExistsGuard]
      },
      {
        path: ':objectiveId/edit',
        component: fromContainers.ObjectiveEditComponent,
        canActivate: [
          fromGuards.ObjectiveExistsGuard,
          fromBoksGuards.BoksGuard,
          fromEndqualificationsGuards.EndqualificationGuard
        ],
        canDeactivate: [fromAuth.CanDeactivateGuard],
        data: {
          title: 'Wijzig Leerdoel'
        }
      }
    ]
  },
  {
    path: fromConstants.BASE_ROUTE + '**',
    redirectTo: fromConstants.BASE_ROUTE
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ObjectiveRoutingModule {}
