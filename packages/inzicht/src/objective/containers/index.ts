import { ObjectiveComponent } from './objective.component'
import { ObjectiveDetailComponent } from './objective-detail/objective-detail.component'
import { ObjectiveListComponent } from './objective-list/objective-list.component'
import { ObjectiveEditComponent } from './objective-edit/objective-edit.component'

export const containers: any[] = [
  ObjectiveComponent,
  ObjectiveDetailComponent,
  ObjectiveListComponent,
  ObjectiveEditComponent
]

export * from './objective.component'
export * from './objective-detail/objective-detail.component'
export * from './objective-edit/objective-edit.component'
export * from './objective-list/objective-list.component'
