import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { Observable } from 'rxjs'
import { NGXLogger } from 'ngx-logger'
import { Store } from '@ngrx/store'

import { ActionsGroup } from '@inzicht/ui'
import * as fromContext from '@inzicht/core/context'
import * as fromAuth from '@inzicht/core'

import * as fromStore from '../../store'
import * as fromConstants from '../../objective.constants'

import { Objective } from '../../models'

@Component({
  selector: 'cv-objective-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './objective-list.component.html'
})
export class ObjectiveListComponent implements OnInit {
  // Class name for logging
  static TAG = ObjectiveListComponent.name

  title = 'Leerdoelen'
  selectedYear$: Observable<string>

  // The core element of this Component
  objectiveElements$: Observable<Objective[]>

  // User authentication role
  userMayEdit$: Observable<boolean>

  columnHeaders = [
    { prop: 'learningLine.name', name: 'Leerlijn', flexGrow: 2 },
    { prop: 'name', name: 'Leerdoel', flexGrow: 8 },
    { prop: 'module.name', name: 'In module', flexGrow: 2 }
  ]

  selected = []

  /**
   * Actions op deze page
   */
  actions: ActionsGroup[] = [
    new ActionsGroup({
      title: 'Acties',
      actions: [
        {
          name: 'Element toevoegen',
          routerLink: 'new'
        },
        {
          name: 'Leerdoel overzicht',
          routerLink: 'todo'
        }
      ]
    }),
    new ActionsGroup({
      title: 'Andere acties',
      actions: [
        {
          name: 'Elementen verwijderen',
          routerLink: 'edit'
        }
      ]
    })
  ]

  constructor(
    private readonly store: Store<fromStore.ObjectiveState>,
    private readonly logger: NGXLogger,
    private readonly router: Router,
    private readonly route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.userMayEdit$ = this.store.select(fromAuth.getIsAuthenticated)
    this.objectiveElements$ = this.store.select(fromStore.getAllObjectives)
    this.selectedYear$ = this.store.select(fromContext.getFullYear)
  }

  onAddElement() {
    this.logger.debug(ObjectiveListComponent.TAG, 'Add element')
    this.router.navigate([fromConstants.BASE_ROUTE, 'new'])
  }

  onSelect({ selected }) {
    if (selected.length === 1) {
      const element = selected[0] as Objective
      this.logger.debug(ObjectiveListComponent.TAG, 'onSelect id = ' + element.id)
      this.router.navigate([element.id], { relativeTo: this.route })
    }
  }
}
