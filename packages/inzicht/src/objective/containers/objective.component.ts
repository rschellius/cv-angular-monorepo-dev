import { Component } from '@angular/core'

@Component({
  selector: 'cv-objective',
  template: `
    <cv-objective-topnav></cv-objective-topnav>
    <div class="app">
      <div class="app__content">
        <div class="app__container">
          <router-outlet></router-outlet>
        </div>
      </div>
    </div>
  `
})
export class ObjectiveComponent {}
