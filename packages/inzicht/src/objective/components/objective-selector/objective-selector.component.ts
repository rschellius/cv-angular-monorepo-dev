import { Component, Input } from '@angular/core'
// import { Observable } from 'rxjs'
import { Objective } from '../../../objective/models'

@Component({
  selector: 'cv-objective-selector',
  template: `
    <ngx-datatable
      class="bootstrap"
      [rows]="objectives"
      [columns]="columnHeaders"
      [columnMode]="'flex'"
      [headerHeight]="50"
      [rowHeight]="'auto'"
      [footerHeight]="50"
      [limit]="5"
      [selected]="selected"
      [selectionType]="'multiClick'"
      (activate)="onActivate($event)"
      (select)="onSelect($event)"
    >
      >
    </ngx-datatable>
  `
})
export class ObjectiveSelectorComponent {
  @Input() objectives: Objective[]

  columnHeaders = [
    { prop: 'learningLine.name', name: 'Leerlijn', flexGrow: 2 },
    { prop: 'module.name', name: 'In module', flexGrow: 2 },
    { prop: 'name', name: 'Naam', flexGrow: 8 }
  ]

  selected = []

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length)
    this.selected.push(...selected)
  }

  onActivate(event) {
    console.log('Activate Event - NOT HANDLED', event)
  }
}
