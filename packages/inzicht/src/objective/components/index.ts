import { ObjectiveFormComponent } from './objective-form/objective-form.component'
import { ObjectiveSelectorComponent } from './objective-selector/objective-selector.component'
import { topnavComponents } from './objective-topnav'

export const components: any[] = [topnavComponents, ObjectiveFormComponent, ObjectiveSelectorComponent]

export * from './objective-topnav'
export * from './objective-form/objective-form.component'
export * from './objective-selector/objective-selector.component'
