import { ObjectiveEffects } from './objective.effect'

export const effects: any[] = [ObjectiveEffects]

export * from './objective.effect'
