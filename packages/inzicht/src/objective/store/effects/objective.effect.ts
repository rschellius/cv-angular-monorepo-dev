import { Injectable } from '@angular/core'
import { HttpParams } from '@angular/common/http'
import { Actions, ofType, Effect } from '@ngrx/effects'
import { switchMap, map, catchError } from 'rxjs/operators'
import { Store } from '@ngrx/store'
import { of } from 'rxjs'

import { AlertInfo } from '@inzicht/core'
import * as fromCore from '@inzicht/core/src/store'
import * as fromRouter from '@inzicht/core/router'
import * as fromContext from '@inzicht/core/context'

import * as objectiveActions from '../actions'
import * as fromServices from '../../services'
import * as fromConstants from '../../objective.constants'

@Injectable()
export class ObjectiveEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<fromContext.ContextState>,
    private readonly objectiveService: fromServices.ObjectiveService
  ) {}

  @Effect()
  loadObjectives$ = this.actions$.pipe(
    ofType(objectiveActions.LOAD_OBJECTIVES),
    switchMap(() => this.store.select(fromContext.getYear)),
    switchMap(year => {
      const params = new HttpParams().set('year', '' + year)
      return this.objectiveService.list(params).pipe(
        map(objectives => new objectiveActions.LoadObjectivesSuccess(objectives)),
        catchError(error => of(new objectiveActions.LoadObjectivesFail(error)))
      )
    })
  )

  @Effect()
  createObjective$ = this.actions$.pipe(
    ofType(objectiveActions.CREATE_OBJECTIVE),
    map((action: objectiveActions.CreateObjective) => action.payload),
    switchMap(fromPayload =>
      this.objectiveService.create(fromPayload).pipe(
        map(objective => new objectiveActions.CreateObjectiveSuccess(objective)),
        catchError(error => of(new objectiveActions.CreateObjectiveFail(error)))
      )
    )
  )

  @Effect()
  updateObjective$ = this.actions$.pipe(
    ofType(objectiveActions.UPDATE_OBJECTIVE),
    map((action: objectiveActions.UpdateObjective) => action.payload),
    switchMap(fromPayload =>
      this.objectiveService.update(fromPayload).pipe(
        map(objective => new objectiveActions.UpdateObjectiveSuccess(objective)),
        catchError(error => of(new objectiveActions.UpdateObjectiveFail(error)))
      )
    )
  )

  @Effect()
  createObjectiveSuccess$ = this.actions$.pipe(
    ofType(objectiveActions.CREATE_OBJECTIVE_SUCCESS, objectiveActions.UPDATE_OBJECTIVE_SUCCESS),
    map((action: objectiveActions.CreateObjectiveSuccess) => action.payload),
    switchMap(objective => [
      new fromCore.AlertSuccess(new AlertInfo('Success', 'Item was aangemaakt.')),
      new fromRouter.Go({
        path: [fromConstants.BASE_ROUTE, objective.id]
      })
    ])
  )

  @Effect()
  deleteObjective$ = this.actions$.pipe(
    ofType(objectiveActions.DELETE_OBJECTIVE),
    map((action: objectiveActions.DeleteObjective) => action.payload),
    switchMap(objective =>
      this.objectiveService.delete(objective.id).pipe(
        // objectiveService.remove returns nothing, so we return
        // the deleted objective ourselves on success
        map(() => new objectiveActions.DeleteObjectiveSuccess(objective)),
        catchError(error => of(new objectiveActions.DeleteObjectiveFail(error)))
      )
    )
  )

  @Effect()
  deleteObjectiveSuccess$ = this.actions$.pipe(
    ofType(objectiveActions.DELETE_OBJECTIVE_SUCCESS),
    switchMap(() => [
      new fromCore.AlertSuccess(new AlertInfo('Success', 'Item was verwijderd.')),
      new fromRouter.Go({
        path: [fromConstants.BASE_ROUTE]
      })
    ])
  )

  @Effect()
  handleOjectivesFail$ = this.actions$.pipe(
    ofType(
      objectiveActions.LOAD_OBJECTIVES_FAIL,
      objectiveActions.CREATE_OBJECTIVE_FAIL,
      objectiveActions.UPDATE_OBJECTIVE_FAIL,
      objectiveActions.DELETE_OBJECTIVE_FAIL
    ),
    map((action: any) => action.payload),
    map(info => new fromCore.AlertError(new AlertInfo(info.title, info.message)))
  )
}
