import { ActionReducerMap, createFeatureSelector } from '@ngrx/store'

import * as fromObjectivees from './objective.reducer'
// import * as fromToppings from './toppings.reducer'

// Hier ontstaat de state tree
export interface ObjectiveState {
  objectiveElements: fromObjectivees.ObjectiveState
  // toppings: fromToppings.ToppingState
}

// De ActionReducerMap zorgt voor typechecking. We kunnen niet zo maar functies toevoegen;
// deze moeten nu uit de Objectivestate komen.
export const reducers: ActionReducerMap<ObjectiveState> = {
  // koppel items aan de reducer function
  objectiveElements: fromObjectivees.reducer
  // toppings: fromToppings.reducer,
}

export const getObjectivesState = createFeatureSelector<ObjectiveState>('objective')
