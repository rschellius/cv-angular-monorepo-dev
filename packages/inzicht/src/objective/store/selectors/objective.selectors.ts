import { createSelector } from '@ngrx/store'

import * as fromRouter from '@inzicht/core/router'
import * as fromFeature from '../reducers'
import * as fromObjectives from '../reducers/objective.reducer'
import { Objective } from '../../models'

//
// Selectors - zijn nodig om door delen van de state tree te navigeren
// They return slices of the state tree.
//
export const getObjectiveState = createSelector(
  fromFeature.getObjectivesState,
  (state: fromFeature.ObjectiveState) => state.objectiveElements
)

export const getObjectivesEntities = createSelector(
  getObjectiveState,
  fromObjectives.getObjectivesEntities
)

// Get the selected item based on id from the route
export const getSelectedObjective = createSelector(
  getObjectivesEntities,
  fromRouter.getRouterState,
  (entities, router): Objective => {
    const params: any[] = router.state.params.filter(item => item['objectiveId'])
    return router.state && params && params[0] && entities[params[0]['objectiveId']]
  }
)

export const getAllObjectives = createSelector(
  getObjectivesEntities,
  entities => {
    // Return an array version of our entities object
    // so that we can iterate over it via ngFor in HTML.
    return Object.keys(entities).map(id => entities[parseInt(id, 10)])
  }
)

export const getObjectivesLoading = createSelector(
  getObjectiveState,
  fromObjectives.getObjectivesLoading
)

export const getObjectivesLoaded = createSelector(
  getObjectiveState,
  fromObjectives.getObjectivesLoaded
)
