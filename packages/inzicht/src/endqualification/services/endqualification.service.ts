import { Injectable } from '@angular/core'
import { Endqualification } from '../models/endqualification.model'
import { EntityService } from '@inzicht/core'
import { environment } from '@inzicht/inzicht'
import { HttpClient } from '@angular/common/http'

const result = {
  A: {
    id: 1,
    tag: 'A',
    area: 'Plan',
    competences: [
      {
        level: 5,
        name: 'Architecture Design',
        proficiency: [
          {
            level: 1,
            sample: 'Sample proficiency description.'
          }
        ]
      },
      {
        level: 6,
        name: 'Application Design',
        proficiency: [
          {
            level: 1,
            sample: 'Sample proficiency description.'
          }
        ]
      },
      {
        level: 8,
        name: 'Sustainable Development',
        proficiency: [
          {
            level: 1,
            sample: 'Sample proficiency description.'
          }
        ]
      }
    ]
  },
  B: {
    id: 2,
    tag: 'B',
    area: 'Build',
    competences: [
      {
        level: 1,
        name: 'Application Development',
        proficiency: [
          {
            level: 1,
            sample: 'Sample proficiency description.'
          }
        ]
      },
      {
        level: 2,
        name: 'Component Integration',
        proficiency: [
          {
            level: 1,
            sample: 'Sample proficiency description.'
          }
        ]
      },
      {
        level: 3,
        name: 'Testing',
        proficiency: [
          {
            level: 1,
            sample: 'Sample proficiency description.'
          }
        ]
      },
      {
        level: 4,
        name: 'Solution Deployment',
        proficiency: [
          {
            level: 1,
            sample: 'Sample proficiency description.'
          }
        ]
      },
      {
        level: 5,
        name: 'Documentation Production',
        proficiency: [
          {
            level: 1,
            sample: 'Sample proficiency description.'
          }
        ]
      },
      {
        level: 6,
        name: 'Systems Engineering',
        proficiency: [
          {
            level: 1,
            sample: 'Sample proficiency description.'
          }
        ]
      }
    ]
  },
  C: {
    id: 3,
    tag: 'C',
    area: 'Run',
    competences: [
      {
        level: 4,
        name: 'Problem Management',
        proficiency: [
          {
            level: 1,
            sample: 'Sample proficiency description.'
          }
        ]
      }
    ]
  },
  D: {
    id: 4,
    tag: 'D',
    area: 'Enable',
    competences: [
      {
        level: 11,
        name: 'Needs Identification',
        proficiency: [
          {
            level: 1,
            sample: 'Sample proficiency description.'
          }
        ]
      }
    ]
  },
  E: {
    id: 5,
    tag: 'E',
    area: 'Manage',
    competences: [
      {
        level: 2,
        name: 'Project and Portfolio Management',
        proficiency: [
          {
            level: 1,
            sample: 'Sample proficiency description.'
          }
        ]
      }
    ]
  },
  F: {
    id: 6,
    tag: 'F',
    area: 'Professional Skills',
    competences: [
      {
        level: 1,
        name: 'Communicatieve Vaardigheden',
        proficiency: [
          {
            level: 1,
            sample: 'Sample proficiency description.'
          }
        ]
      },
      {
        level: 2,
        name: 'Leervaardigheden',
        proficiency: [
          {
            level: 1,
            sample: 'Sample proficiency description.'
          }
        ]
      },
      {
        level: 3,
        name: 'Oordeelsvorming & Onderzoekend Vermogen',
        proficiency: [
          {
            level: 1,
            sample: 'Sample proficiency description.'
          }
        ]
      },
      {
        level: 4,
        name: 'Samenwerken & Omgevingsbewustzijn',
        proficiency: [
          {
            level: 1,
            sample: 'Sample proficiency description.'
          }
        ]
      }
    ]
  }
}

@Injectable()
export class EndqualificationService extends EntityService<Endqualification> {
  constructor(httpClient: HttpClient) {
    super(httpClient, environment.API_BASE_URL, 'ecf')
  }
}
