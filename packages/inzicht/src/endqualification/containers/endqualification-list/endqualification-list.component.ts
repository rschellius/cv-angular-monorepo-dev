import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { Observable, of } from 'rxjs'
import { Store } from '@ngrx/store'

import { Endqualification } from '../../models'
import * as fromStore from '../../store'

@Component({
  selector: 'cv-endqualification-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './endqualification-list.component.html'
})
export class EndqualificationListComponent implements OnInit {
  // Class name for logging
  static TAG = EndqualificationListComponent.name

  title = 'Eindkwalificaties'

  // The core element of this Component
  endqualifications$: Observable<Endqualification[]>

  columnHeaders = [
    { prop: 'id', name: 'Id', flexGrow: 1 },
    { prop: 'areaTag', name: 'Tag', flexGrow: 1 },
    { prop: 'areaName', name: 'Area', flexGrow: 2 },
    { prop: 'competenceLevel', name: 'CompLevel', flexGrow: 1 },
    { prop: 'competenceName', name: 'Competence', flexGrow: 4 },
    { prop: 'proficiencyLevel', name: 'Proficiency', flexGrow: 1 }
  ]

  selected = []

  constructor(
    private readonly store: Store<fromStore.EndqualificationState>,
    private readonly router: Router,
    private readonly route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.endqualifications$ = this.store.select(fromStore.getAllEndqualifications)
  }

  onSelect({ selected }) {
    if (selected.length === 1) {
      const element = selected[0] as Endqualification
      this.router.navigate([element.id], { relativeTo: this.route })
    }
  }
}
