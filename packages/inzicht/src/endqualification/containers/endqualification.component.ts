import { Component } from '@angular/core'

@Component({
  selector: 'cv-endqualification',
  template: `
    <cv-endqualification-topnav></cv-endqualification-topnav>
    <div class="app">
      <div class="app__content">
        <div class="app__container">
          <router-outlet></router-outlet>
        </div>
      </div>
    </div>
  `
})
export class EndqualificationComponent {}
