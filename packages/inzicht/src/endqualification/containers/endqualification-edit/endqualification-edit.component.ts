import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { ActionsGroup } from '@inzicht/ui'
import { Observable } from 'rxjs'
import { tap } from 'rxjs/operators'
import { Store } from '@ngrx/store'

import * as fromStore from '../../store'
import { Endqualification } from '../../models'

@Component({
  selector: 'cv-endqualification-edit',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './endqualification-edit.component.html',
  styleUrls: []
})
export class EndqualificationEditComponent implements OnInit {
  title: string

  //  to be edited
  endqualification$: Observable<Endqualification>

  // Option to select element as learning line.
  isLearningLine: boolean

  /**
   * Actions op deze page
   */
  actions: ActionsGroup[] = [
    new ActionsGroup({
      title: 'Acties',
      actions: [
        {
          name: 'Annuleren',
          routerLink: '..'
        }
      ]
    })
  ]

  constructor(
    private readonly store: Store<fromStore.EndqualificationState>,
    private readonly route: ActivatedRoute,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.title =
      this.route.snapshot.data['title'] || 'Edit Leerdoel node element'
    this.isLearningLine =
      this.route.snapshot.data['optionalLearningline'] || false

    this.endqualification$ = this.store
      .select(fromStore.getSelectedEndqualification)
      .pipe(
        // tap(console.log),
        tap((endqualification: Endqualification = null) => {
          const endqualificationExists = !!endqualification
          // const toppings = endqualificationExists
          //   ? endqualification.toppings.map(topping => topping.id)
          //   : []
          // this.store.dispatch(new fromStore.VisualizeToppings(toppings))
        })
      )
    // this.toppings$ = this.store.select(fromStore.getAllToppings)
    // this.visualise$ = this.store.select(fromStore.getPizzaVisualized)
  }

  onSelect(event: number[]) {
    // this.store.dispatch(new fromStore.VisualizeToppings(event))
  }

  onCreate(event: Endqualification) {
    this.store.dispatch(new fromStore.CreateEndqualification(event))
  }

  onUpdate(event: Endqualification) {
    console.log('onUpdate')
    this.store.dispatch(new fromStore.UpdateEndqualification(event))
  }

  onRemove(event: Endqualification) {
    const remove = window.confirm('Are you sure?')
    if (remove) {
      this.store.dispatch(new fromStore.DeleteEndqualification(event))
    }
  }

  onCancel() {
    this.router.navigate(['..'], { relativeTo: this.route })
  }
}
