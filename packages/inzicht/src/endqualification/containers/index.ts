import { EndqualificationComponent } from './endqualification.component'
import { EndqualificationDetailComponent } from './endqualification-detail/endqualification-detail.component'
import { EndqualificationListComponent } from './endqualification-list/endqualification-list.component'
import { EndqualificationEditComponent } from './endqualification-edit/endqualification-edit.component'

export const containers: any[] = [
  EndqualificationComponent,
  EndqualificationDetailComponent,
  EndqualificationListComponent,
  EndqualificationEditComponent
]

export * from './endqualification.component'
export * from './endqualification-detail/endqualification-detail.component'
export * from './endqualification-edit/endqualification-edit.component'
export * from './endqualification-list/endqualification-list.component'
