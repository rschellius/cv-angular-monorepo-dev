import { Action } from '@ngrx/store'
import { Endqualification } from '../../models'

//
// Load actions
//
export const LOAD_ENDQUALIFICATIONS = '[Endqualifications] Load Endqualifications'
export const LOAD_ENDQUALIFICATIONS_FAIL = '[Endqualifications] Load Endqualifications Fail'
export const LOAD_ENDQUALIFICATIONS_SUCCESS = '[Endqualifications] Load Endqualifications Success'

// Action creators
export class LoadEndqualifications implements Action {
  readonly type = LOAD_ENDQUALIFICATIONS
}

export class LoadEndqualificationsFail implements Action {
  readonly type = LOAD_ENDQUALIFICATIONS_FAIL
  constructor(public payload: any) {}
}

export class LoadEndqualificationsSuccess implements Action {
  readonly type = LOAD_ENDQUALIFICATIONS_SUCCESS
  constructor(public payload: Endqualification[]) {}
}

//
// Create Actions
//
export const CREATE_ENDQUALIFICATION = '[Endqualifications] Create Endqualification'
export const CREATE_ENDQUALIFICATION_FAIL = '[Endqualifications] Create Endqualification Fail'
export const CREATE_ENDQUALIFICATION_SUCCESS = '[Endqualifications] Create Endqualification Success'

// Action creators
export class CreateEndqualification implements Action {
  readonly type = CREATE_ENDQUALIFICATION
  constructor(public payload: Endqualification) {}
}

export class CreateEndqualificationFail implements Action {
  readonly type = CREATE_ENDQUALIFICATION_FAIL
  constructor(public payload: any) {}
}

export class CreateEndqualificationSuccess implements Action {
  readonly type = CREATE_ENDQUALIFICATION_SUCCESS
  constructor(public payload: Endqualification) {}
}

//
// Update Actions
//
export const UPDATE_ENDQUALIFICATION = '[Endqualifications] Update Endqualification'
export const UPDATE_ENDQUALIFICATION_FAIL = '[Endqualifications] Update Endqualification Fail'
export const UPDATE_ENDQUALIFICATION_SUCCESS = '[Endqualifications] Update Endqualification Success'

// Action creators
export class UpdateEndqualification implements Action {
  readonly type = UPDATE_ENDQUALIFICATION
  constructor(public payload: Endqualification) {}
}

export class UpdateEndqualificationFail implements Action {
  readonly type = UPDATE_ENDQUALIFICATION_FAIL
  constructor(public payload: any) {}
}

export class UpdateEndqualificationSuccess implements Action {
  readonly type = UPDATE_ENDQUALIFICATION_SUCCESS
  constructor(public payload: Endqualification) {}
}

//
// Delete Actions
//
export const DELETE_ENDQUALIFICATION = '[Endqualifications] Delete Endqualification'
export const DELETE_ENDQUALIFICATION_FAIL = '[Endqualifications] Delete Endqualification Fail'
export const DELETE_ENDQUALIFICATION_SUCCESS = '[Endqualifications] Delete Endqualification Success'

// Action creators
export class DeleteEndqualification implements Action {
  readonly type = DELETE_ENDQUALIFICATION
  constructor(public payload: Endqualification) {}
}

export class DeleteEndqualificationFail implements Action {
  readonly type = DELETE_ENDQUALIFICATION_FAIL
  constructor(public payload: any) {}
}

export class DeleteEndqualificationSuccess implements Action {
  readonly type = DELETE_ENDQUALIFICATION_SUCCESS
  constructor(public payload: Endqualification) {}
}

// action types
export type EndqualificationsAction =
  | LoadEndqualifications
  | LoadEndqualificationsFail
  | LoadEndqualificationsSuccess
  | CreateEndqualification
  | CreateEndqualificationFail
  | CreateEndqualificationSuccess
  | UpdateEndqualification
  | UpdateEndqualificationFail
  | UpdateEndqualificationSuccess
  | DeleteEndqualification
  | DeleteEndqualificationFail
  | DeleteEndqualificationSuccess
