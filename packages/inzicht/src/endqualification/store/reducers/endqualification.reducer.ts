import * as fromEndqualification from '../actions'
import { Endqualification } from '../../models'

//
// EndqualificationState interface
//
export interface EndqualificationState {
  entities: {
    [id: number]: Endqualification
  }
  loaded: boolean
  loading: boolean
}

//
// Initialisation
//
export const initialState: EndqualificationState = {
  entities: {},
  loaded: false,
  loading: false
}

//
//
//
export function reducer(
  state: EndqualificationState = initialState,
  action: fromEndqualification.EndqualificationsAction
): EndqualificationState {
  switch (action.type) {
    case fromEndqualification.LOAD_ENDQUALIFICATIONS: {
      return {
        ...state,
        loading: true
      }
    }

    case fromEndqualification.LOAD_ENDQUALIFICATIONS_SUCCESS: {
      const endqualificationElements = action.payload
      const entities = endqualificationElements.reduce(
        (
          items: { [id: number]: Endqualification },
          endqualificationElement: Endqualification
          // items, endqualificationElement
        ) => {
          return {
            ...items,
            [endqualificationElement.id]: endqualificationElement
          }
        },
        {
          ...state.entities
        }
      )
      return {
        ...state,
        loading: false,
        loaded: true,
        entities
      }
    }

    case fromEndqualification.LOAD_ENDQUALIFICATIONS_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: false
      }
    }

    default: {
      return state
    }
  }
}

// Selector functions: get the pieces of our state that we need
export const getEndqualificationsEntities = (state: EndqualificationState) => state.entities
export const getEndqualificationsLoading = (state: EndqualificationState) => state.loading
export const getEndqualificationsLoaded = (state: EndqualificationState) => state.loaded
