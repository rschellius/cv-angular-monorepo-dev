import { ActionReducerMap, createFeatureSelector } from '@ngrx/store'

import * as fromEndqualifications from './endqualification.reducer'
// import * as fromToppings from './toppings.reducer'

// Hier ontstaat de state tree
export interface EndqualificationState {
  endqualificationElements: fromEndqualifications.EndqualificationState
  // toppings: fromToppings.ToppingState
}

// De ActionReducerMap zorgt voor typechecking. We kunnen niet zo maar functies toevoegen;
// deze moeten nu uit de Endqualificationstate komen.
export const reducers: ActionReducerMap<EndqualificationState> = {
  // koppel items aan de reducer function
  endqualificationElements: fromEndqualifications.reducer
  // toppings: fromToppings.reducer,
}

export const getEndqualificationsState = createFeatureSelector<EndqualificationState>('endqualification')
