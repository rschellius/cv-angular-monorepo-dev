/**
 *
 */
import { Entity } from '@inzicht/core'

export interface Area {
  areaTag: string
  areaName: string
}

export interface Competence {
  competenceLevel: number
  competenceName: string
}

export interface Proficiency {
  proficiencyLevel: number
}

export interface Qualification {
  areaTag: string
  areaName: string
  competenceLevel: number
  competenceName: string
  proficiencyLevel: number
}

export class Endqualification extends Entity {
  areaTag: string
  areaName: string
  areaDescription: string
  competenceLevel: number
  competenceName: string
  competenceDescription: string
  proficiencyLevel: number
  proficiencySample: string

  constructor(values: any) {
    super(values)
    try {
      this.areaTag = values['areaTag']
      this.areaName = values['areaName']
      this.areaDescription = values['areaDescription']
      this.competenceLevel = values['competenceLevel']
      this.competenceName = values['competenceName']
      this.competenceDescription = values['competenceDescription']
      this.proficiencyLevel = values['proficiencyLevel']
      this.proficiencySample = values['proficiencySample']
    } catch (e) {
      console.error('Error in Course constructor: ' + e.toString())
    }
  }
}
