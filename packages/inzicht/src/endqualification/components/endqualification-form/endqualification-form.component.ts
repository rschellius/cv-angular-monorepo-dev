import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  OnChanges,
  SimpleChanges,
  ChangeDetectionStrategy
} from '@angular/core'
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'

import { Endqualification } from '../../../endqualification/models'

@Component({
  selector: 'cv-endqualification-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './endqualification-form.component.html'
})
export class EndqualificationFormComponent implements OnInit, OnChanges {
  exists = false

  @Input() endqualification: Endqualification
  @Input() isLearningLine = false

  @Output() selected = new EventEmitter<Endqualification>()
  @Output() create = new EventEmitter<Endqualification>()
  @Output() update = new EventEmitter<Endqualification>()
  @Output() remove = new EventEmitter<Endqualification>()
  @Output() cancel = new EventEmitter<void>()

  // Demo: Hardcoded docentnamen; zouden van de API moeten komen.
  persons = [
    { name: 'Ruud Hermans' },
    { name: 'Pascal van Gastel' },
    { name: 'Jan Montizaan' },
    { name: 'Gitta de Vaan' },
    { name: 'Eefje Gijzen' },
    { name: 'Erco Argante' },
    { name: 'Arno Broeders' },
    { name: 'Robin Schellius' },
    { name: 'Johan Smarius' },
    { name: 'Peter Vos' },
    { name: 'Heidie Tops' },
    { name: 'Ger Oosting' },
    { name: 'Evert Jan de Voogt' },
    { name: 'Gerard Wagenaar' }
  ]

  form = this.fb.group({
    name: ['', Validators.required],
    shortDescription: ['', Validators.required],
    longDescription: [''],
    administrativeRemark: [''],
    learningLine: [[]],
    qualifications: [[]],
    module: ['']
  })

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    console.log('onInit', this.endqualification)
  }

  get nameControl() {
    return this.form.get('name') as FormControl
  }

  get nameControlInvalid() {
    return this.nameControl.hasError('required') && this.nameControl.touched
  }

  get shortDescriptionControl() {
    return this.form.get('shortDescription') as FormControl
  }

  get shortDescriptionControlInvalid() {
    return this.shortDescriptionControl.hasError('required') && this.shortDescriptionControl.touched
  }

  get longDescriptionControl() {
    return this.form.get('longDescription') as FormControl
  }

  get longDescriptionControlInvalid() {
    return this.longDescriptionControl.hasError('required') && this.longDescriptionControl.touched
  }

  get administrativeRemark() {
    return this.form.get('administrativeRemark') as FormControl
  }

  get learningLineControl() {
    return this.form.get('learningLine') as FormControl
  }

  get moduleControl() {
    return this.form.get('module') as FormControl
  }

  get qualificationsControl() {
    return this.form.get('qualifications') as FormControl
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.endqualification && this.endqualification.id) {
      this.exists = true
      this.form.patchValue(this.endqualification)
    }
    // this.form
    //   .get('toppings')
    //   .valueChanges.pipe(
    //     map(toppings => toppings.map((topping: Topping) => topping.id))
    //   )
    //   .subscribe(value => this.selected.emit(value))
  }

  createItem(form: FormGroup) {
    console.log('createItem')
    const { value, valid } = form
    if (valid) {
      this.create.emit(value)
    }
  }

  updateItem(form: FormGroup) {
    const { value, valid, touched } = form
    if (touched && valid) {
      this.update.emit({ ...this.endqualification, ...value })
    }
  }

  removeItem(form: FormGroup) {
    const { value } = form
    this.remove.emit({ ...this.endqualification, ...value })
  }

  onCancel() {
    this.cancel.emit()
  }
}
