import { Component, Input, ChangeDetectionStrategy } from '@angular/core'
import { Qualification } from '../../../endqualification/models'

@Component({
  selector: 'cv-endqualification-display',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <ngx-datatable
      class="bootstrap"
      [rows]="qualifications"
      [columns]="columnHeaders"
      [columnMode]="'flex'"
      [headerHeight]="50"
      [rowHeight]="50"
    >
    </ngx-datatable>
  `
})
export class EndqualificationDisplayComponent {
  @Input()
  qualifications: Qualification[]

  columnHeaders = [
    { prop: 'areaTag', name: 'Area', flexGrow: 1 },
    { prop: 'areaName', name: 'Name', flexGrow: 3 },
    { prop: 'competenceLevel', name: 'Comp.', flexGrow: 1 },
    { prop: 'competenceName', name: 'Name', flexGrow: 3 },
    { prop: 'proficiencyLevel', name: 'Proficiency', flexGrow: 2 }
  ]
}
