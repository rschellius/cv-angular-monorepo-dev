import { EndqualificationFormComponent } from './endqualification-form/endqualification-form.component'
import { EndqualificationSelectorComponent } from './endqualification-selector/endqualification-selector.component'
import { EndqualificationDisplayComponent } from './endqualification-display/endqualification-display.component'
import { topnavComponents } from './endqualification-topnav'

export const components: any[] = [
  topnavComponents,
  EndqualificationDisplayComponent,
  EndqualificationFormComponent,
  EndqualificationSelectorComponent
]

export * from './endqualification-topnav'
export * from './endqualification-display/endqualification-display.component'
export * from './endqualification-form/endqualification-form.component'
export * from './endqualification-selector/endqualification-selector.component'
