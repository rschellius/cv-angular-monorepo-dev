import { Component, OnInit } from '@angular/core'
import { Store } from '@ngrx/store'

import * as fromContext from '@inzicht/core/context'
import { Observable } from 'rxjs'

@Component({
  selector: 'cv-dashboard',
  templateUrl: './dashboard.component.html'
})
export class AppDashboardComponent implements OnInit {
  title = 'Dashboard'

  year$: Observable<number> | undefined
  fullYear$: Observable<string | null> | undefined

  constructor(private readonly store: Store<fromContext.ContextState>) {}

  ngOnInit() {
    this.year$ = this.store.select(fromContext.getYear)
    this.fullYear$ = this.store.select(fromContext.getFullYear)
  }

  onYearSelected(year: number) {
    console.log('onYearSelected', year)
    this.store.dispatch(new fromContext.ChangeYear({ year: year }))
  }
}
