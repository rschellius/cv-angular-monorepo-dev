import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { Router, NavigationStart, NavigationEnd } from '@angular/router'
import { Location, PopStateEvent } from '@angular/common'
import { Store } from '@ngrx/store'

import { AuthState } from '@inzicht/core/src/store'
import * as fromActions from '@inzicht/core/src/store/actions'
import * as fromContext from '@inzicht/core/context'

@Component({
  selector: 'app-root',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['main.component.scss'],
  templateUrl: './main.component.html'
})
export class MainComponent implements OnInit {
  title = 'Curriculum inZicht'

  constructor(
    private store: Store<AuthState>,
    private context: Store<fromContext.ContextState>,
    private router: Router,
    private location: Location
  ) {}

  private lastPoppedUrl?: string
  private yScrollStack: number[] = []

  ngOnInit() {
    this.store.dispatch(new fromActions.IsLoggedIn())
    this.context.dispatch(new fromContext.LoadContext())

    //
    // https://stackoverflow.com/questions/39601026/angular-2-scroll-to-top-on-route-change
    //
    // this.location.subscribe((ev: PopStateEvent) => {
    //   this.lastPoppedUrl = ev.url
    // })
    // this.router.events.subscribe((ev: any) => {
    //   if (ev instanceof NavigationStart) {
    //     if (ev.url !== this.lastPoppedUrl) {
    //       this.yScrollStack.push(window.scrollY)
    //     }
    //   } else if (ev instanceof NavigationEnd) {
    //     if (ev.url === this.lastPoppedUrl) {
    //       this.lastPoppedUrl = undefined
    //       window.scrollTo(0, this.yScrollStack.pop())
    //     } else {
    //       window.scrollTo(0, 0)
    //     }
    //   }
    // })
  }
}
