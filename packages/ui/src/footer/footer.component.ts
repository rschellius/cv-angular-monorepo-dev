import { Component, OnInit } from '@angular/core'
import { Observable, of } from 'rxjs'
import { Store } from '@ngrx/store'

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent {
  isLoggedIn$: Observable<boolean>
  pageLoaded$: Observable<boolean>

  constructor() {}

  ngOnInit() {
    this.isLoggedIn$ = of(true)
  }
}
