import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { RouterModule } from '@angular/router'

import * as fromCore from '.'

@NgModule({
  imports: [RouterModule, NgbModule, CommonModule],
  declarations: [...fromCore.components],
  exports: [...fromCore.components]
})
export class UIModule {}
