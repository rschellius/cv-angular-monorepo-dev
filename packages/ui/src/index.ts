import { MainMenuComponent } from './mainmenu/mainmenu.component'
import { FooterComponent } from './footer/footer.component'
import { PageHeaderComponent } from './pageheader/pageheader.component'
import { ActionsComponent } from './asides/actions/actions.component'
import { FilterComponent } from './asides/filter/filter.component'
import { FilterActiveComponent } from './asides/filter-active/filter-active.component'

export const components: any[] = [
  FooterComponent,
  MainMenuComponent,
  PageHeaderComponent,
  ActionsComponent,
  FilterComponent,
  FilterActiveComponent
]

export * from './asides/actions/actions.component'
export * from './asides/actions/actions.model'
export * from './asides/filter/filter.component'
export * from './asides/filter-active/filter-active.component'
export * from './mainmenu/mainmenu.component'
export * from './footer/footer.component'
export * from './pageheader/pageheader.component'
