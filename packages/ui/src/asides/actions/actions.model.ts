/**
 * Actions
 *
 * Definieert een class voor met modelleren van acties op emtiteiten. Deze kunnen op het scherm
 * worden getoond in een aside element.
 *
 */
interface ActionsGroupInterface {
  title: string
  actions: [
    {
      name: string
      routerLink: string
      authGuard: boolean | undefined
    }
  ]
}

export class ActionsGroup implements ActionsGroupInterface {
  title!: string
  actions!: [
    {
      name: string
      routerLink: string
      authGuard: boolean | undefined
    }
  ]

  constructor(values: any) {
    try {
      this.title = values.title
      this.actions = values.actions
      // if (values && values.roles && values.roles.length > 0) {
      // values.roles.forEach(role => this.roles.push(role));
      // }
    } catch (e) {
      console.error(e.toString())
    }
  }
}
