import { Component, Input, ChangeDetectionStrategy } from '@angular/core'
import { DndNode } from '../../models'

@Component({
  selector: 'dnd-node-card',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="card__container">
      <p class="card__title">{{ item.name }} ({{ item.id }})</p>
      <div class="card__content">
        <dnd-node-card *ngFor="let childNode of item.childNodes" [item]="childNode"></dnd-node-card>
      </div>
      <div class="dnd-container" [dragula]="'BOKS_CONTENT'" [dragulaModel]="item.contentNodes" [id]="item.id">
        <dnd-content-card
          *ngFor="let content of item.contentNodes"
          [content]="content.name"
        ></dnd-content-card>
      </div>
    </div>
  `,
  styleUrls: ['dnd-node-card.component.scss', '../../../assets/css/dragula.css']
})
export class DndNodeCardComponent {
  @Input() item: DndNode
}
