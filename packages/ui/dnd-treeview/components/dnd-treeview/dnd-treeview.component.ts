import { Component, OnInit, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core'
import { DndTree } from '../../models/dnd-tree.model'
import { BoksContentElement } from 'packages/inzicht/src/boks/models'

import { Subscription } from 'rxjs'
import { DragulaService } from 'ng2-dragula'

@Component({
  selector: 'dnd-treeview',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="dnd_tree__container">
      <div *ngFor="let node of tree">
        <dnd-node-card [item]="node"></dnd-node-card>
      </div>
    </div>
  `,
  styleUrls: ['dnd-treeview.component.scss', '../../../assets/css/dragula.css']
})
export class DndTreeViewComponent {
  @Input() tree: DndTree

  @Output() onDrop = new EventEmitter<{ item: BoksContentElement; targetNodeId: number }>()

  // RxJS Subscription is an excellent API for managing many unsubscribe calls.
  subs = new Subscription()

  constructor(private dragulaService: DragulaService) {}

  ngOnInit() {
    this.subs.add(
      this.dragulaService.dropModel('BOKS_CONTENT').subscribe(({ item, target }) => {
        // console.log('Dropping', item.name, 'id =', item.id, ' on target.id', target.id)
        this.onDrop.emit({ item: item, targetNodeId: parseInt(target.id, 10) })
      })
    )
  }

  ngOnDestroy() {
    // destroy all the subscriptions at once
    this.subs.unsubscribe()
  }
}
