import { Component, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core'
import { DndContent } from '../../models/dnd-content.model'

@Component({
  selector: 'dnd-content-card',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="content__container">
      <p>{{ content }} ({{ content.id }})</p>
    </div>
  `,
  styleUrls: ['dnd-content-card.component.scss', '../../../assets/css/dragula.css']
})
export class DndContentCardComponent {
  @Input() content: DndContent
}
