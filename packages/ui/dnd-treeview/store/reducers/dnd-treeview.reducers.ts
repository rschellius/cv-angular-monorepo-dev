import * as fromActions from '../actions'

export interface YearState {
  year: number
  fullYear: string | null
  loaded: boolean
  loading: boolean
}

export const initialState: YearState = {
  year: 0,
  fullYear: null,
  loaded: false,
  loading: false
}

export function reducer(state: YearState = initialState, action: fromActions.ContextActions): YearState {
  switch (action.type) {
    //
    //
    //
    case fromActions.CHANGE_YEAR:
    case fromActions.LOAD_CONTEXT: {
      return {
        ...state,
        loading: true
      }
    }

    //
    //
    //
    // case fromActions.CHANGE_YEAR_SUCCESS:
    // case fromActions.LOAD_CONTEXT_SUCCESS: {
    //   const { year } = action.payload
    //   const nextYear = +year + 1
    //   return {
    //     ...state,
    //     year: year,
    //     fullYear: '' + year + '-' + nextYear,
    //     loading: false,
    //     loaded: true
    //   }
    // }

    //
    //
    //
    case fromActions.CHANGE_YEAR_FAIL:
    case fromActions.LOAD_CONTEXT_FAIL: {
      return {
        ...state,
        year: 0,
        fullYear: '[Geen jaar geselecteerd]',
        loading: false,
        loaded: true
      }
    }

    //
    //
    //
    default: {
      return state
    }
  }
}

// Selector functions: get the pieces of our state that we need
export const getYearState = (state: YearState) => state
export const getYearStateLoading = (state: YearState) => state.loading
export const getYearStateLoaded = (state: YearState) => state.loaded
export const getYear = (state: YearState) => state.year
export const getFullYear = (state: YearState) => state.fullYear
