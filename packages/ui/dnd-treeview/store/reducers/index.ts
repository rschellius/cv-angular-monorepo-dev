import * as fromState from './dnd-treeview.reducers'
import { ActionReducerMap, createFeatureSelector } from '@ngrx/store'

// Hier ontstaat de state tree
export interface ContextState {
  year: fromState.YearState
}

// De ActionReducerMap zorgt voor typechecking. We kunnen niet zo maar functies toevoegen;
// deze moeten nu uit de Authtate komen.
export const reducers: ActionReducerMap<ContextState> = {
  // koppel users aan de reducer function
  year: fromState.reducer
}

export const getContextState = createFeatureSelector<ContextState>('context')
