import { Action } from '@ngrx/store'
// import { Context } from '../../models'

export const LOAD_CONTEXT = '[Context] Load Context'
export const LOAD_CONTEXT_FAIL = '[Context] Load Context Fail'
export const LOAD_CONTEXT_SUCCESS = '[Context] Load Context Success'

// Action creators
export class LoadContext implements Action {
  readonly type = LOAD_CONTEXT
}

export class LoadContextFail implements Action {
  readonly type = LOAD_CONTEXT_FAIL
  constructor(public payload: any) {}
}

export class LoadContextSuccess implements Action {
  readonly type = LOAD_CONTEXT_SUCCESS
  constructor(/*public payload: Context*/) {}
}

export const CHANGE_YEAR = '[Context] Change Year'
export const CHANGE_YEAR_FAIL = '[Context] Change Year Fail'
export const CHANGE_YEAR_SUCCESS = '[Context] Change Year Success'

// Action creators
export class ChangeYear implements Action {
  readonly type = CHANGE_YEAR
  constructor(public payload: any) {}
}

export class ChangeYearFail implements Action {
  readonly type = CHANGE_YEAR_FAIL
  constructor(public payload: any) {}
}

export class ChangeYearSuccess implements Action {
  readonly type = CHANGE_YEAR_SUCCESS
  constructor(/*public payload: Context*/) {}
}

export type ContextActions =
  | LoadContextSuccess
  | LoadContextFail
  | LoadContext
  | ChangeYear
  | ChangeYearSuccess
  | ChangeYearFail
