import { DndContent } from './dnd-content.model'

/**
 *
 */
export interface DndNode {
  id: number
  name: string
  childNodes: DndNode[]
  contentNodes: DndContent[]
}
