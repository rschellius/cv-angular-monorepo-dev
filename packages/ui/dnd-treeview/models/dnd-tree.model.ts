import { DndNode } from './dnd-node.model'

/**
 *
 */
export interface DndTree {
  items: DndNode[]
}
