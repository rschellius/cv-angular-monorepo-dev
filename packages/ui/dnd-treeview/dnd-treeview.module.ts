import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { DragulaModule } from 'ng2-dragula'

import * as fromComponents from './components'

@NgModule({
  imports: [FormsModule, ReactiveFormsModule, DragulaModule.forRoot(), CommonModule],
  providers: [],
  declarations: [...fromComponents.components],
  exports: [...fromComponents.components]
})
export class DndTreeviewModule {}
