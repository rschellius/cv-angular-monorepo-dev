module.exports = {
	deleteDestPath: !process.env.WATCH_MODE,
	lib: {
		entryFile: "public-api.ts",
		cssUrl: "inline",
		umdModuleIds: {
			// vendors
			tslib: "tslib",
			lodash: "_",

			// local
			"@inzicht/package-1": "inzicht.package-1",
			"@inzicht/package-1/testing": "inzicht.package-1.testing"
		}
	},
	whitelistedNonPeerDependencies: ["."]
};
